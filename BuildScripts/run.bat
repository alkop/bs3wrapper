@echo off
setlocal

echo:
echo:clean, build, pack. . .
echo:---------------------------------

set SCRIPTDIR="%~dp0"
echo:
echo:Switching to %SCRIPTDIR% . . .
cd /d %SCRIPTDIR%  

call clean
call :Check

call build
rem call :Check

call pack
call :Check

echo:------------
echo:All Success.

goto:eof

:Check

if %ERRORLEVEL% EQU 123 (
	echo:"* Strange MSBuild error 123 that could be ignored."
	exit /b
)

if ERRORLEVEL 1 (
	echo:
	echo:ERROR: One of steps is failed with ERRORLEVEL==%ERRORLEVEL%!
	exit 1
) else ( 
	exit /b
)
