@echo off
pushd ".."

set NUGET=".nuget\NuGet.exe"
set PACKAGEDIR="bin\NuGetPackages"

echo:
echo:Packing NuGet packages into %PACKAGEDIR% . . .

if exist %PACKAGEDIR% rd /s /q %PACKAGEDIR%
md %PACKAGEDIR% 

echo:
call :ParseVersion "BS3Wrapper\Properties\AssemblyInfo.cs"
echo:================
%NUGET% pack "NuGet\BS3Wrapper.nuspec" -OutputDirectory %PACKAGEDIR% -NonInteractive

echo: 
echo:Packaging succeeded.
popd

goto:eof

:ParseVersion
set VERFILE=%~1
for /f "usebackq tokens=2,3 delims=:() " %%A in ("%VERFILE%") do (
	if "%%A"=="AssemblyInformationalVersion" set VER=%%~B
)
exit /b