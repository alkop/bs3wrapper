﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.WebPages;

namespace BS3Wrapper.Extensions
{
    public static class HtmlExtensions
    {
        public static HelperResult Join<T>(this IEnumerable<T> list, Func<T, HelperResult> template, Func<T, HelperResult> separator)
        {
            var first = true;
            var result = new HelperResult(writer =>
            {
                foreach (var item in list)
                {
                    if (first == false)
                    {
                        separator(item).WriteTo(writer);
                    }
                    first = false;
                    template(item).WriteTo(writer);
                }
            });

            return result;
        }

        public static string Nbsp(int count = 1)
        {
            string result = string.Empty;
            for (int i = 0; i < count; i++)
            {
                result += Constants.Nbsp;
            }
            return result;
        }

        public static bool IsClassAttribute(this string attribute)
        {
            return attribute != null && attribute.ToLower() == "class";
        }

        public static IDictionary<string, object> AppendClass(this IDictionary<string, object> attributes, string @class, bool replace = false)
        {
            return attributes.AppendAttribute("class", @class, replace);
        }

        public static IDictionary<string, object> AppendAttribute(this IDictionary<string, object> attributes, string key, string value, bool replace = false)
        {
            attributes = attributes ?? new Dictionary<string, object>();
            if (replace)
            {
                attributes[key] = value;
            }
            else
            {
                object attr;
                if (attributes.TryGetValue(key, out attr))
                {
                    attributes[key] = value + " " + attr;
                }
                else
                {
                    attributes[key] = value;
                }
            }
            return attributes;
        }

        public static HtmlTag Append(this HtmlTag tag, string text, bool accessible = true)
        {
            if (accessible)
            {
                tag = tag.Append(text);
            }
            return tag;
        }

        public static HtmlTag Class(this HtmlTag tag, string @class, bool apply = true)
        {
            if (apply)
            {
                tag = tag.Class(@class);
            }
            return tag;
        }

        public static HtmlTag NotEmptyClass(this HtmlTag tag, string @class)
        {
            if (@class.HasValue())
            {
                tag = tag.Class(@class);
            }
            return tag;
        }

        public static HtmlTag NotEmptyId(this HtmlTag tag, string id)
        {
            if (id.HasValue())
            {
                tag = tag.Id(id);
            }
            return tag;
        }

        public static void RegisterScript(this HtmlHelper html, string script)
        {
            var ctx = html.ViewContext.HttpContext;
            var scripts = ctx.Items[Constants.ScriptsKey] as Stack<string>;
            if (scripts == null)
            {
                scripts = new Stack<string>();
                ctx.Items[Constants.ScriptsKey] = scripts;
            }
            if (!scripts.Contains(script))
            {
                scripts.Push(script);
            }
        }
    }
}