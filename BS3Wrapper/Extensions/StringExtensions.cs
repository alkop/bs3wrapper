﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Data.SqlTypes;
using System.Net;
using System.Security;
using System.Web;

namespace BS3Wrapper.Extensions
{
    public static class StringExtensions
    {
        public static string FormatBy(this string s, params object[] args)
        {
            return string.Format(s, args);
        }

        public static string FormatBy(this string s, IFormatProvider provider, params object[] args)
        {
            return string.Format(provider, s, args);
        }

        public static bool HasValue(this string value)
        {
            return string.IsNullOrWhiteSpace(value) == false;
        }

        public static string EmptyIfNull(this string value)
        {
            return value ?? string.Empty;
        }

        public static string Truncate(this string value, int limit)
        {
            return (value.HasValue() && value.Length > limit) ? string.Concat(value.Substring(0, Math.Min(value.Length, limit)), "...") : value;
        }

        public static T To<T>(this string value, T defaultValue) where T : struct
        {
            var type = typeof(T);
            if (value != null)
            {
                var parse = type.GetMethod("TryParse", new[] { typeof(string), type.MakeByRefType() });
                var parameters = new object[] { value, default(T) };
                if ((bool)parse.Invoke(null, parameters))
                    return (T)parameters[1];
            }
            return defaultValue;
        }

        public static T To<T>(this string value) where T : struct
        {
            return value.To(default(T));
        }

        public static DateTime? ToDateTime(this string value)
        {
            return ToDateTime(value, null);
        }

        public static DateTime? ToDateTime(this string value, DateTime? defaultValue)
        {
            DateTime result;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return defaultValue;
        }

        public static bool IsInteger(this string input)
        {
            int val;
            return int.TryParse(input, out val);
        }

        public static bool IsDecimal(this string input)
        {
            decimal val;
            return decimal.TryParse(input, out val);
        }

        public static bool IsDecimal(this string input, int precision, int scale)
        {
            decimal val;
            return decimal.TryParse(input, out val) && new SqlDecimal(val).Precision <= precision && new SqlDecimal(val).Scale <= scale;
        }

        public static bool IsValidUrl(this string text)
        {
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?");
            return rx.IsMatch(text);
        }

        public static bool IsValidEmailAddress(this string email)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(email);
        }

        public static string HtmlEncode(this string data)
        {
            return HttpUtility.HtmlEncode(data);
        }

        public static string HtmlDecode(this string data)
        {
            return HttpUtility.HtmlDecode(data);
        }

        public static string UrlEncode(this string url)
        {
            return HttpUtility.UrlEncode(url);
        }

        public static string UrlDecode(this string url)
        {
            return HttpUtility.UrlDecode(url);
        }

        public static System.Collections.Specialized.NameValueCollection ParseQueryString(this string query)
        {
            return HttpUtility.ParseQueryString(query);
        }

        public static string ToRelativeUrl(this string url)
        {
            return VirtualPathUtility.ToAppRelative(url);
        }

        public static string Quote(this string text)
        {
            return "'{0}'".FormatBy(text);
        }
    }
}