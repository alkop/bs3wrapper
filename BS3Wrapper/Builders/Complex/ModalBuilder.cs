﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.IO;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Enums;
    using Extensions;

    public class ModalBuilder<TModel> : IModalBuilder
    {
        private readonly TextWriter htmlWriter;

        public ModalBuilder(HtmlHelper<TModel> htmlHelper, string id = null, string @class = null, ModalSize size = ModalSize.Medium)
        {
            htmlWriter = htmlHelper.ViewContext.Writer;

            htmlWriter.Write(HtmlTags.Div.NotEmptyId(id)
                                    .Class("modal fade")
                                    .NotEmptyClass(@class)
                                    .Attribute("tabindex", "-1")
                                    .Attribute("role", "dialog")
                                    .Attribute("aria-hidden", "true")
                                    .ToHtml(TagRenderMode.StartTag));

            htmlWriter.Write(HtmlTags.Div.Class("modal-dialog")
                                         .Class(size.GetClass())
                                         .ToHtml(TagRenderMode.StartTag));

            htmlWriter.Write(HtmlTags.Div.Class("modal-content").ToHtml(TagRenderMode.StartTag));
        }

        public ISectionBuilder Header()
        {
            return new ModalSectionBuilder(ModalSection.Header, htmlWriter);
        }

        public ISectionBuilder Header(string @class)
        {
            return new ModalSectionBuilder(ModalSection.Header, htmlWriter, @class);
        }

        public ISectionBuilder Body()
        {
            return new ModalSectionBuilder(ModalSection.Body, htmlWriter);
        }
        public ISectionBuilder Body(string id)
        {
            return new ModalSectionBuilder(ModalSection.Body, htmlWriter, id:id);
        }

        public ISectionBuilder Footer()
        {
            return new ModalSectionBuilder(ModalSection.Footer, htmlWriter);
        }

        public void Dispose()
        {
            for (int i = 0; i < 3; i++)
            {
                htmlWriter.Write(HtmlTags.Div.ToHtml(TagRenderMode.EndTag));
            }
        }
    }
}