/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Web;

namespace BS3Wrapper.Builders.Complex.Contracts
{
    using Enums;
    using Models;

    public interface INavBarBuilder : IHtmlString
    {
        /// <summary>
        /// Modify the look of the navbar by adding .navbar-inverse
        /// </summary>
        INavBarBuilder InverseTheme();
        /// <summary>
        /// Set url of root page (link on brand name)
        /// </summary>
        INavBarBuilder HomeUrl(string url);
        /// <summary>
        /// Set / Change brand (project) name
        /// </summary>
        INavBarBuilder BrandName(string name);
        INavBarBuilder BrandName(string name, BrandLogoViewModel logo);
        INavBarBuilder BrandLogo(BrandLogoType type, string source, int? size = null);
        /// <summary>
        /// Apply .container to center and pad navbar content
        /// </summary>
        INavBarBuilder Centered();
        INavBarBuilder StaticTop();
        INavBarBuilder FixedTop();
        INavBarBuilder FixedBottom();
        INavBarBuilder EnableOpenDropDownMenuOnHover();
        INavBarBuilder MenuItem(MenuItemViewModel item);
        INavBarBuilder MenuItem(string name, string url, bool accessible = true);
        INavBarBuilder MenuItem(string name, string url, string icon = null, bool accessible = true);
        INavBarBuilder MenuItem(string name, string action, string controller, string area, string icon = null, bool accessible = true);
        IAccountManagerBuilder AccountManager(string name, bool accessible = true);
        IAccountManagerBuilder AccountManager(string name, string role, bool accessible = true);
        INavBarDropDownMenuBuilder DropDownMenu(string name, string icon = null, bool accessible = true);
        INavBarBuilder ShowOnlineUsers(IEnumerable<OnlineUsersViewModel> users, bool accessible = true);
        INavBarBuilder ShowOnlineUsers(int count, bool accessible = true);
    }
}