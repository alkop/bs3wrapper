﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Models;

    public class NavBarDropDownMenuBuilder : INavBarDropDownMenuBuilder
    {
        private readonly IList<MenuItemViewModel> items;
        private readonly INavBarBuilder navbar;
        private readonly string ddMenuName;
        private readonly string ddMenuIcon;
        private readonly bool ddAccessible;

        public NavBarDropDownMenuBuilder(string ddMenuName, string ddMenuIcon, INavBarBuilder navbar, bool ddAccessible = true)
        {
            this.navbar = navbar;
            this.ddMenuName = ddMenuName;
            this.ddMenuIcon = ddMenuIcon;
            this.ddAccessible = ddAccessible;
            items = new List<MenuItemViewModel>();
        }

        public INavBarBuilder Ready()
        {
            var menuItem = new MenuItemViewModel { Name = ddMenuName, HasChilds = true, Childs = items, Icon = ddMenuIcon, IsAccessible = true };
            return ddAccessible ? navbar.MenuItem(menuItem) : navbar;
        }

        public INavBarDropDownMenuBuilder MenuItem(string name, string url, bool accessible = true)
        {
            return MenuItem(name, url, null, accessible);
        }

        public INavBarDropDownMenuBuilder MenuItem(string name, string url, string icon = null, bool accessible = true)
        {
            items.Add(new MenuItemViewModel { Name = name, Url = url, Icon = icon, IsAccessible = accessible });
            return this;
        }

        public INavBarDropDownMenuBuilder MenuItem(string name, string action, string controller, string area, string icon = null, bool accessible = true)
        {
            var url = string.Concat(controller, "/", action);
            if (!string.IsNullOrEmpty(area))
            {
                url = string.Concat(area, "/", url);
            }
            return MenuItem(name, url, icon, accessible);
        }

        public string ToHtmlString()
        {
            throw new NotImplementedException();
        }
    }
}
