﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Core.Base;
    using Enums;
    using Extensions;

    public class ProgressBarBuilder : BaseBuilder<IProgressBarBuilder>, IProgressBarBuilder
    {
        private string label;
        private int value, minValue, maxValue;
        private ProgressBarType type;
        private bool isStriped, isAnimated;

        public ProgressBarBuilder() : this(string.Empty) { }

        public ProgressBarBuilder(ProgressBarType type) : this(0, type) { }

        public ProgressBarBuilder(string label) : this(0, label) { }

        public ProgressBarBuilder(string label, ProgressBarType type) : this(0, label, type) { }

        public ProgressBarBuilder(int value) : this(value, string.Empty) { }

        public ProgressBarBuilder(int value, ProgressBarType type) : this(value, string.Empty, type) { }

        public ProgressBarBuilder(int value, string label) : this(value, label, ProgressBarType.Default) { }

        public ProgressBarBuilder(int value, string label, ProgressBarType type) : base(HtmlTags.Div.Attribute("role", "progressbar"))
        {
            this.value = value;
            this.label = label;
            this.type = type;
            minValue = 0;
            maxValue = 100;
        }

        public IProgressBarBuilder Type(ProgressBarType barType)
        {
            type = barType;
            return this;
        }

        public IProgressBarBuilder Striped()
        {
            isStriped = true;
            return this;
        }

        public IProgressBarBuilder Animated()
        {
            isStriped = isAnimated = true;
            return this;
        }

        public IProgressBarBuilder Label(string text)
        {
            label = text;
            return this;
        }

        public IProgressBarBuilder CurrentValue(int current)
        {
            value = current;
            return this;
        }

        public IProgressBarBuilder MinValue(int min)
        {
            minValue = min;
            return this;
        }

        public IProgressBarBuilder MaxValue(int max)
        {
            maxValue = max;
            return this;
        }

        public string ToHtmlString()
        {
            HtmlTag.Class(type.GetClass())
                   .Class("active", isAnimated)
                   .Class("progress-bar-striped", isStriped)
                   .Attribute("aria-valuemin", minValue.ToString())
                   .Attribute("aria-valuemax", maxValue.ToString())
                   .Style("width", value + "%");

            if (label.HasValue())
            {
                HtmlTag.Append(label);
            }
            else
            {
                HtmlTag.Append(HtmlTags.Span.Class("sr-only")
                                            .Append(value + "% Complete"));
            }

            return HtmlTags.Div.Class("progress")
                               .Append(HtmlTag)
                               .ToString();
        }
    }
}