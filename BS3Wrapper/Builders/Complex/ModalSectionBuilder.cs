﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.IO;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Enums;
    using Extensions;

    public class ModalSectionBuilder : ISectionBuilder
    {
        private readonly TextWriter htmlWriter;
        private readonly ModalSection section;

        internal ModalSectionBuilder(ModalSection section, TextWriter htmlWriter, string @class = null, string id = null)
        {
            this.section = section;
            this.htmlWriter = htmlWriter;
            htmlWriter.Write(HtmlTags.Div.NotEmptyId(id).Class(section.GetClass()).NotEmptyClass(@class).ToHtml(TagRenderMode.StartTag));
            if (section == ModalSection.Header)
            {
                htmlWriter.Write(HtmlTags.Button.Class("close")
                                                .Data("dismiss", "modal")
                                                .Attribute("aria-label", "Close")
                                                .Append(HtmlTags.Span.Attribute("aria-hidden", "true")
                                                                     .Append("&times;")));
                htmlWriter.Write(HtmlTags.H4.Class("modal-title").ToHtml(TagRenderMode.StartTag));
            }
        }

        public void Dispose()
        {
            if (section == ModalSection.Header)
            {
                htmlWriter.Write(HtmlTags.H4.ToHtml(TagRenderMode.EndTag));
            }
            htmlWriter.Write(HtmlTags.Div.ToHtml(TagRenderMode.EndTag));
        }
    }
}