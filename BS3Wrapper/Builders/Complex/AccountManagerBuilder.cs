﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Enums;
    using Extensions;
    using Models;

    public class AccountManagerBuilder : IAccountManagerBuilder
    {
        private readonly AccountManagerViewModel accountManager;
        private readonly INavBarBuilder navBar;

        public AccountManagerBuilder(string name, string role, INavBarBuilder navBar)
        {
            accountManager = new AccountManagerViewModel { Name = name, Role = role };
            this.navBar = navBar;
        }

        public INavBarBuilder Ready()
        {
            return navBar;
        }

        public IAccountManagerBuilder Photo(string url)
        {
            return Photo(url, accountManager.Name);
        }

        public IAccountManagerBuilder Photo(string url, string alt)
        {
            accountManager.PhotoUrl = url;
            accountManager.PhotoAlt = alt;
            return this;
        }

        public IAccountManagerBuilder Photo(string url, int width, int height)
        {
            return Photo(url, accountManager.Name, width, height);
        }

        public IAccountManagerBuilder Photo(string url, string alt, int width, int height)
        {
            accountManager.PhotoUrl = url;
            accountManager.PhotoAlt = alt;
            accountManager.PhotoWidth = width;
            accountManager.PhotoHeight = height;
            return this;
        }

        public IAccountManagerBuilder SignOut(string url, ButtonType type = ButtonType.Info, ButtonSize buttonSize = ButtonSize.Default)
        {
            return SignOut(url, "Sign Out", type, buttonSize);
        }

        public IAccountManagerBuilder SignOut(string url, string name, ButtonType type = ButtonType.Info, ButtonSize buttonSize = ButtonSize.Default)
        {
            accountManager.SignOutUrl = url;
            accountManager.SignOutBtnName = name;
            accountManager.SignOutBtnType = type;
            accountManager.SignOutBtnSize = buttonSize;
            return this;
        }

        public IAccountManagerBuilder Profile(string url, ButtonType type = ButtonType.Info, ButtonSize buttonSize = ButtonSize.Default)
        {
            return Profile(url, "View Profile", type, buttonSize);
        }

        public IAccountManagerBuilder Profile(string url, string name, ButtonType type = ButtonType.Info, ButtonSize buttonSize = ButtonSize.Default)
        {
            accountManager.ProfileUrl = url;
            accountManager.ProfileBtnName = name;
            accountManager.ProfileBtnType = type;
            accountManager.ProfileBtnSize = buttonSize;
            return this;
        }

        public string ToHtmlString()
        {
            var rowTag = HtmlTags.Div.Class("row");

            bool hasPhoto = !string.IsNullOrWhiteSpace(accountManager.PhotoUrl);

            if (hasPhoto)
            {
                var photoContainer = HtmlTags.Div.Class("col-md-5");
                var imgTag = HtmlTags.Img.Attribute("src", accountManager.PhotoUrl)
                                         .Class("img-responsive").Style("padding-bottom", "5px");

                if (accountManager.PhotoWidth.HasValue)
                {
                    imgTag.Attribute("width", accountManager.PhotoWidth.ToString());
                }

                if (accountManager.PhotoHeight.HasValue)
                {
                    imgTag.Attribute("height", accountManager.PhotoHeight.ToString());
                }
                photoContainer.Append(imgTag);
                rowTag.Append(photoContainer);
            }

            var viewProfileTag = HtmlTags.Div.Class(hasPhoto ? "col-md-7" : "col-md-12")
                                             .Append(HtmlTags.P.Class("text-muted")
                                                               .Append(HtmlTags.Strong.Append(accountManager.Name)))
                                             .Append(HtmlTags.P.Class("text-muted small")
                                                               .Append(accountManager.Role))
                                             .Append(HtmlTags.Div.Class("divider"));
            if (!string.IsNullOrWhiteSpace(accountManager.ProfileUrl))
            {
                viewProfileTag.Append(HtmlTags.A.Class(accountManager.ProfileBtnType.GetClass())
                                                .Class(accountManager.ProfileBtnSize.GetClass())
                                                .Append(accountManager.ProfileBtnName)
                                                .Href(accountManager.ProfileUrl));
            }

            rowTag.Append(viewProfileTag);

            HtmlTag footerTag = null;

            if (!string.IsNullOrWhiteSpace(accountManager.SignOutUrl))
            {
                footerTag = HtmlTags.Div.Style("background-color", "#DDD")
                                    .Append(HtmlTags.Div.Style("padding", "15px 15px 15px 15px")
                                                    .Append(HtmlTags.Div.Class("row").Style("margin", "1px")
                                                                    .Append(HtmlTags.Div.Class("pull-right")
                                                                                        .Append(HtmlTags.A.Class(accountManager.SignOutBtnSize.GetClass())
                                                                                                          .Class(accountManager.SignOutBtnType.GetClass())
                                                                                                          .Attribute("href", accountManager.SignOutUrl)
                                                                                                          .Append(accountManager.SignOutBtnName)))));
            }

            var profile = HtmlTags.Li.Class("dropdown")
                                    .Append(HtmlTags.A.Attribute("href", "#")
                                                        .Class("dropdown-toggle badge")
                                                        .Style("line-height", "10px")
                                                        .Style("margin-top", "3px")
                                                        .Data("toggle", "dropdown")
                                                        .Append(HtmlTags.Span.Class("glyphicon glyphicon-user"))
                                                        .Append(HtmlTags.Strong.Append(accountManager.Name).Style("padding", "0 3px"))
                                                        .Append(HtmlTags.Span.Class("glyphicon glyphicon-chevron-down")))
                                    .Append(HtmlTags.Ul.Class("dropdown-menu").Style("padding-bottom", "0px")
                                                        .Append(HtmlTags.Li
                                                                        .Append(HtmlTags.Div.Style("width", "320px")
                                                                                            .Style("padding", "15px")
                                                                                            .Style("padding-bottom", "0px")
                                                                                            .Append(rowTag))
                                                                        .Append(footerTag ?? HtmlTags.Hr)));
            return profile.ToString();
        }

        public override string ToString()
        {
            return ToHtmlString();
        }
    }
}
