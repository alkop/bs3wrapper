﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.IO;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Enums;
    using Extensions;

    public class PanelBuilder<TModel> : IPanelBuilder
    {
        private readonly TextWriter htmlWriter;

        private PanelType type;
        private string id;
        private string @class;

        private bool beginTagIsWritten;

        public PanelBuilder(HtmlHelper<TModel> htmlHelper, string id = null, string @class = null){
            htmlWriter = htmlHelper.ViewContext.Writer;
            this.id = id;
            this.@class = @class;
            type = PanelType.Default;
        }

        public void GetBeginHtml()
        {
            htmlWriter.Write(HtmlTags.Div.Class(@class)
                                         .Class(type.GetClass())
                                         .Id(id)
                                         .ToHtml(TagRenderMode.StartTag));

            beginTagIsWritten = true;
        }

        public IPanelBuilder SetType(PanelType customType)
        {
            type = customType;
            return this;
        }

        public IPanelBuilder SetId(string customId)
        {
            id = customId;
            return this;
        }

        public IPanelBuilder SetClass(string customClass)
        {
            @class = customClass;
            return this;
        }

        public ISectionBuilder Header()
        {
            if (!beginTagIsWritten)
            {
                GetBeginHtml();
            }

            return new PanelSectionBuilder(PanelSection.Header, htmlWriter);
        }

        public ISectionBuilder Body()
        {
            if (!beginTagIsWritten)
            {
                GetBeginHtml();
            }

            return new PanelSectionBuilder(PanelSection.Body, htmlWriter);
        }

        public ISectionBuilder Footer()
        {
            if (!beginTagIsWritten)
            {
                GetBeginHtml();
            }

            return new PanelSectionBuilder(PanelSection.Footer, htmlWriter);
        }

        public void Dispose()
        {
            htmlWriter.Write(HtmlTags.Div.ToHtml(TagRenderMode.EndTag));
        }
    }
}