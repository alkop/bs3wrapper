﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Complex
{
    using Contracts;
    using Enums;
    using Extensions;
    using Helpers;
    using Models;

    public class NavBarBuilder<TModel> : INavBarBuilder
    {
        private readonly HtmlHelper<TModel> html;
        private string brand, theme, position, container, homeUrl;
        private BrandLogoViewModel logo;
        private readonly IList<MenuItemViewModel> rootMenu;
        private IAccountManagerBuilder accountManager;
        private bool isAccountManagerAccessible, showOnlineUsers;
        private IEnumerable<OnlineUsersViewModel> onlineUsers;

        public NavBarBuilder(HtmlHelper<TModel> html) : this(html, string.Empty) { }

        public NavBarBuilder(HtmlHelper<TModel> html, string brand) : this(html, brand, null) { }

        public NavBarBuilder(HtmlHelper<TModel> html, string brand, BrandLogoViewModel logo)
        {
            this.html = html;
            this.brand = brand;
            this.logo = logo;
            homeUrl = "/";
            theme = "navbar-default";
            container = "container-fluid";
            position = string.Empty;
            rootMenu = new List<MenuItemViewModel>();
        }

        public INavBarBuilder BrandName(string name)
        {
            brand = name;
            return this;
        }

        public INavBarBuilder BrandName(string name, BrandLogoViewModel brandLogo)
        {
            brand = name;
            logo = brandLogo;
            return this;
        }

        public INavBarBuilder BrandLogo(BrandLogoType type, string source, int? size = null)
        {
            logo = new BrandLogoViewModel { BrandLogoType = type, Source = source, Size = size };
            return this;
        }

        public INavBarBuilder InverseTheme()
        {
            theme = "navbar-inverse";
            return this;
        }

        public INavBarBuilder Centered()
        {
            container = "container";
            return this;
        }

        public INavBarBuilder StaticTop()
        {
            position = "navbar-static-top";
            return this;
        }

        public INavBarBuilder FixedTop()
        {
            position = "navbar-fixed-top";
            return this;
        }

        public INavBarBuilder FixedBottom()
        {
            position = "navbar-fixed-bottom";
            return this;
        }

        public INavBarBuilder EnableOpenDropDownMenuOnHover()
        {
            html.RegisterScript(JavaScriptHelper.EnableOpenDropDownMenuOnHover());
            return this;
        }

        public INavBarBuilder HomeUrl(string url)
        {
            homeUrl = url;
            return this;
        }

        public INavBarBuilder MenuItem(MenuItemViewModel item)
        {
            rootMenu.Add(item);
            return this;
        }

        public INavBarBuilder MenuItem(string name, string url, bool accessible = true)
        {
            return MenuItem(name, url, null, accessible);
        }

        public INavBarBuilder MenuItem(string name, string url, string icon = null, bool accessible = true)
        {
            rootMenu.Add(new MenuItemViewModel { Name = name, Url = url, Icon = icon, IsAccessible = accessible });
            return this;
        }

        public INavBarBuilder MenuItem(string name, string action, string controller, string area, string icon = null, bool accessible = true)
        {
            var url = string.Concat(controller, "/", action);
            if (!string.IsNullOrEmpty(area))
            {
                url = string.Concat(area, "/", url);
            }
            return MenuItem(name, url, icon);
        }

        public IAccountManagerBuilder AccountManager(string name, bool accessible = true)
        {
            return AccountManager(name, null, accessible);
        }

        public IAccountManagerBuilder AccountManager(string name, string role, bool accessible = true)
        {
            accountManager = new AccountManagerBuilder(name, role, this);
            isAccountManagerAccessible = accessible;
            return accountManager;
        }

        public INavBarDropDownMenuBuilder DropDownMenu(string name, string icon = null, bool accessible = true)
        {
            return new NavBarDropDownMenuBuilder(name, icon, this, accessible);
        }

        public INavBarBuilder ShowOnlineUsers(IEnumerable<OnlineUsersViewModel> users, bool accessible = true)
        {
            showOnlineUsers = accessible;
            onlineUsers = users ?? new List<OnlineUsersViewModel>();
            return this;
        }

        public INavBarBuilder ShowOnlineUsers(int count, bool accessible = true)
        {
            var users = new List<OnlineUsersViewModel>();
            for (int i = 0; i < count; i++)
            {
                users.Add(new OnlineUsersViewModel());
            }
            return ShowOnlineUsers(users, accessible);
        }

        public string ToHtmlString()
        {
            var collapseTag = HtmlTags.Button.Class("navbar-toggle collapsed")
                                             .Data("toggle", "collapse")
                                             .Data("target", "#navbar")
                                             .Attribute("aria-expanded", "false")
                                             .Attribute("aria-controls", "navbar")
                                             .Append(HtmlTags.Span.Class("sr-only")
                                                                  .Append("Navigation"));

            for (int i = 0; i < 3; i++)
            {
                collapseTag.Append(HtmlTags.Span.Class("icon-bar"));
            }

            var logoTag = HtmlTags.Div.Class("logo");
            if (logo != null)
            {
                if (logo.BrandLogoType == BrandLogoType.Image)
                {
                    var imageTag = HtmlTags.Img.Attribute("src", logo.Source);
                    if (logo.Size.HasValue)
                    {
                        imageTag.Attribute("height", logo.Size.ToString());
                    }
                    logoTag.Append(imageTag);
                }
                else
                {
                    var iconTag = HtmlTags.I.Class(logo.Source);
                    if (logo.Size.HasValue)
                    {
                        iconTag.Attribute("style", "font-size:" + logo.Size + "px");
                    }
                    logoTag.Append(iconTag);
                }
                logoTag.Append(HtmlExtensions.Nbsp());
            }
            onlineUsers = onlineUsers ?? new List<OnlineUsersViewModel>();
            var currentOnlineUsers = onlineUsers as OnlineUsersViewModel[] ?? onlineUsers.ToArray();
            var online = HtmlTags.Li.Class("online-users")
                                    .Append(HtmlTags.A.Href("#")
                                                    .Class("dropdown-toggle")
                                                    .Data("toggle", "dropdown")
                                                    .Attribute("role", "button")
                                                    .Attribute("aria-expanded", "false")
                                                    .Append("ONLINE:" + HtmlExtensions.Nbsp())
                                                    .Append(HtmlTags.Span.Id("online").Class("badge").Append(currentOnlineUsers.Length.ToString())));
            var users = HtmlTags.Ul.Class("dropdown-menu dropdown-online-users").Attribute("role", "menu");
            foreach (var user in currentOnlineUsers)
            {
                var userLi = HtmlTags.Li.Append(HtmlTags.Span.Class("user").Append(HtmlTags.Span.Class("user-info-left")
                                                                                                .Append(HtmlTags.I.Class("glyphicon glyphicon-user"))
                                                                                                .Append(HtmlTags.Span.Class("user-info")
                                                                                                                      .Append(HtmlTags.Span.Class("online-user-name")
                                                                                                                                            .Append(user.Login))
                                                                                                                      .Append(HtmlTags.Span.Class("online-user-position")
                                                                                                                                           .Append(user.Position)))));
                users.Append(userLi);
            }
            online.Append(users);
            return HtmlTags.Div.Class("navbar")
                               .Class(position)
                               .Class(theme)
                               .Append(HtmlTags.Div.Class(container)
                                                   .Append(HtmlTags.Div.Class("navbar-header")
                                                                       .Append(collapseTag)
                                                                       .Append(HtmlTags.A.Class("navbar-brand")
                                                                                         .Href(homeUrl)
                                                                                         .Append(logoTag.Append(brand))))
                                                   .Append(HtmlTags.Div.Class("collapse navbar-collapse")
                                                                       .Attribute("id", "navbar")
                                                                       .Append(BuildMenu(rootMenu))
                                                                       .Append(HtmlTags.Ul.Class("nav navbar-nav navbar-right")
                                                                                          .Append(online.ToString(), showOnlineUsers)
                                                                                          .Append(accountManager.ToString(), isAccountManagerAccessible)))).ToString();
        }

        public string BuildMenu(IList<MenuItemViewModel> menu, bool isSubMenu = false)
        {
            if (menu.Any())
            {
                var menuTag = HtmlTags.Ul.Class(isSubMenu ? "dropdown-menu" : "nav navbar-nav");
                foreach (var menuItem in menu)
                {
                    if (!menuItem.IsAccessible) continue;
                    var menuItemTag = HtmlTags.Li;
                    var linkTag = HtmlTags.A.Href(menuItem.Url ?? "#");
                    var iconTag = menuItem.Icon.HasValue() ? HtmlTags.I.Class(menuItem.Icon) + HtmlExtensions.Nbsp() : string.Empty;
                    if (menuItem.HasChilds)
                    {
                        linkTag.Class("dropdown-toggle")
                               .Data("toggle", "dropdown")
                               .Attribute("role", "button")
                               .Attribute("aria-haspopup", "true")
                               .Attribute("aria-expanded", "true")
                               .Append(iconTag + menuItem.Name)
                               .Append(HtmlExtensions.Nbsp() + HtmlTags.Span.Class("caret"));
                        menuItemTag.Append(linkTag + BuildMenu(menuItem.Childs, true));
                    }
                    else
                    {
                        linkTag.Append(iconTag + menuItem.Name);
                        menuItemTag.Append(linkTag);
                    }
                    menuTag.Append(menuItemTag);
                }
                return menuTag.ToString();
            }
            return string.Empty;
        }
    }
}
