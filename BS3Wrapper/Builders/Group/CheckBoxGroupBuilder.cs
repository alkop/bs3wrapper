﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Group
{
    using Base;
    using Contracts;
    using Core.Contracts;

    public class CheckBoxGroupBuilder<TModel> : BaseGroupBuilder<ICheckBoxGroupBuilder, ICheckBoxBuilder, TModel, bool>, ICheckBoxGroupBuilder
    {
        public CheckBoxGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, bool>> expression) : base(html, expression, html.BS3().CheckBoxFor(expression)) { }

        public ICheckBoxGroupBuilder Checked(bool @checked)
        {
            Control.Checked(@checked);
            return this;
        }

        public ICheckBoxGroupBuilder Label(string label)
        {
            Control.Label(label);
            return this;
        }
    }
}
