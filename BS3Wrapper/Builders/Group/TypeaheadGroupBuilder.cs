/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Group
{
    using Base;
    using Contracts;
    using Core.Contracts;

    public class TypeaheadGroupBuilder<TModel, TKeyProperty, TTextProperty> : BaseTextBoxGroupBuilder<ITypeaheadGroupBuilder, ITypeaheadBuilder, TModel, TTextProperty>, ITypeaheadGroupBuilder
    {
        public TypeaheadGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, TTextProperty>> expression) : base(html, expression, html.BS3().TypeaheadFor(expression)) { }

        public TypeaheadGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, TKeyProperty>> ctrlKeyExpression, Expression<Func<TModel, TTextProperty>> ctrlTextExpression) : base(html, ctrlTextExpression, html.BS3().TypeaheadFor(ctrlKeyExpression, ctrlTextExpression)) { }

        public ITypeaheadGroupBuilder Source(string url)
        {
            Control.Source(url);
            return this;
        }

        public ITypeaheadGroupBuilder Delay(int delay)
        {
            Control.Delay(delay);
            return this;
        }

        public ITypeaheadGroupBuilder Items(int items)
        {
            Control.Items(items);
            return this;
        }

        public ITypeaheadGroupBuilder ShowHintOnFocus()
        {
            Control.ShowHintOnFocus();
            return this;
        }

        public ITypeaheadGroupBuilder Matcher(string function)
        {
            Control.Matcher(function);
            return this;
        }

        public ITypeaheadGroupBuilder Sorter(string function)
        {
            Control.Sorter(function);
            return this;
        }

        public ITypeaheadGroupBuilder Updater(string function)
        {
            Control.Updater(function);
            return this;
        }

        public ITypeaheadGroupBuilder Highlighter(string function)
        {
            Control.Highlighter(function);
            return this;
        }

        public ITypeaheadGroupBuilder DisplayText(string function)
        {
            Control.DisplayText(function);
            return this;
        }
    }
}