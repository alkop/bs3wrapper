﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BS3Wrapper.Builders.Group.Base
{
    using Attributes.Validation.Base;
    using Core.Base;
    using Core.Contracts.Base.Input;
    using Enums;
    using Extensions;

    public abstract class BaseGroupBuilder<TGroupBuilder, TControlBuilder, TModel, TProperty> : BaseInputBuilder<TGroupBuilder, TModel> where TGroupBuilder : class
                                                                                                                                        where TControlBuilder : IBaseInputBuilder<TControlBuilder>
    {
        protected readonly TControlBuilder Control;
        protected readonly string ControlId;
        protected readonly Expression<Func<TModel, TProperty>> Expression;

        protected string CtrlLabel;
        protected Width[] CtrlWidth = { Width.ColSm5 };
        protected Width[] LblWidth = { Width.ColSm2 };
        protected IDictionary<string, object> AttributesOfGroup;
        protected object ObjectValue;
        protected bool CtrlAsStaticTag;

        protected BaseGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, TControlBuilder control) : base(html, control.Tag)
        {
            Control = control;
            Expression = expression;
            ControlId = html.IdFor(expression).ToHtmlString();
            CtrlLabel = html.DisplayNameFor(expression).ToHtmlString();
            var meta = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            ObjectValue = meta.Model ?? string.Empty;
            AttributesOfGroup = new Dictionary<string, object>();
        }

        public virtual TGroupBuilder ControlWidth(params Width[] width)
        {
            CtrlWidth = width;
            return this as TGroupBuilder;
        }

        public virtual TGroupBuilder LabelWidth(params Width[] width)
        {
            LblWidth = width;
            return this as TGroupBuilder;
        }

        public virtual TGroupBuilder ControlLabel(string name)
        {
            CtrlLabel = name;
            return this as TGroupBuilder;
        }

        public virtual TGroupBuilder GroupAttributes(object htmlAttributes)
        {
            AttributesOfGroup = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            return this as TGroupBuilder;
        }

        protected void InjectValidation(HtmlTag group)
        {
            var memberExpression = Expression.Body as MemberExpression;
            if (memberExpression != null)
            {
                var validationAttributes = memberExpression.Member.GetCustomAttributes(typeof(IGroupClientValidation), false)
                                                                  .Cast<IGroupClientValidation>().ToList();
                var label = CtrlLabel.Quote();
                foreach (var attribute in validationAttributes)
                {
                    attribute.InjectHtmlAttributes(group, label);
                }
            }
        }

        public virtual TGroupBuilder ControlAsStaticText()
        {
            CtrlAsStaticTag = true;            
            return this as TGroupBuilder;
        }

        public virtual string ToHtmlString()
        {
            var controlTag = HtmlTags.Div;
            controlTag.Append(CtrlAsStaticTag ? HtmlTags.P.Class("form-control-static")
                      .Append(ObjectValue.ToString()).ToString() : Control.ToHtmlString());

            foreach (var width in CtrlWidth.Where(width => width != Width.None))
            {
                controlTag.Class(width.GetClass());
            }

            var labelTag = HtmlTags.Label.Class("control-label")
                                         .Attribute("for", ControlId)
                                         .Append(CtrlLabel);

            foreach (var width in LblWidth.Where(width => width != Width.None))
            {
                labelTag.Class(width.GetClass());
            }

            var groupTag = HtmlTags.Div.Class("form-group")
                                       .Data("control", ControlId);

            if (AttributesOfGroup != null)
            {
                foreach (var attribute in AttributesOfGroup)
                {
                    if (attribute.Key.IsClassAttribute())
                    {
                        groupTag.Class(attribute.Value.ToString());
                    }
                    else
                    {
                        groupTag.Attribute(attribute.Key, attribute.Value.ToString());
                    }
                }
            }

            InjectValidation(groupTag);

            groupTag.Append(labelTag).Append(controlTag);

            return groupTag.ToString();
        }
    }
}
