﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Group.Base
{
    using Contracts.Base;
    using Core.Contracts.Base.Input;
    using Enums;

    public abstract class BaseTextBoxGroupBuilder<TGroupBuilder, TBuilder, TModel, TProperty> : BaseGroupBuilder<TGroupBuilder, TBuilder, TModel, TProperty>,
                                                                                                IBaseGroupBuilder<TGroupBuilder>, IBaseTextBoxBuilder<TGroupBuilder>
        where TGroupBuilder : class
        where TBuilder : IBaseTextBoxBuilder<TBuilder>
    {
        protected BaseTextBoxGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression) : base(html, expression, (TBuilder)html.BS3().TextBoxFor(expression)) { }

        protected BaseTextBoxGroupBuilder(HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, TBuilder builder) : base(html, expression, builder) { }

        public TGroupBuilder Format(string format)
        {
            return this as TGroupBuilder;
        }

        public TGroupBuilder Autocomplete(Autocomplete type)
        {
            Control.Autocomplete(type);
            return this as TGroupBuilder;
        }

        public TGroupBuilder MaxLength(int length)
        {
            Control.MaxLength(length);
            return this as TGroupBuilder;
        }

        public TGroupBuilder Placeholder(string text)
        {
            Control.Placeholder(text);
            return this as TGroupBuilder;
        }

        public TGroupBuilder TabIndex(int index)
        {
            Control.TabIndex(index);
            return this as TGroupBuilder;
        }

        public TGroupBuilder Size(InputSize inputSize)
        {
            Control.Size(inputSize);
            return this as TGroupBuilder;
        }
    }
}
