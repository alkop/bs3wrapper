﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Linq;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;

    public class AlertBuilder : BaseBuilder<IAlertBuilder>, IAlertBuilder
    {
        private readonly List<string> messageSequence;
        private AlertType messageType;
        private bool isClosable;

        public AlertBuilder() : this(string.Empty) { }

        public AlertBuilder(string message) : this(message, AlertType.Info) { }

        public AlertBuilder(string message, object htmlAttributes) : this(message, AlertType.Info, htmlAttributes) { }

        public AlertBuilder(string message, AlertType type) : this(message, type, false) { }

        public AlertBuilder(string message, AlertType type, object htmlAttributes) : this(message, type, false, htmlAttributes) { }

        public AlertBuilder(string message, bool closable) : this(message, AlertType.Info, closable, null) { }

        public AlertBuilder(string message, bool closable, object htmlAttributes) : this(message, AlertType.Info, closable, htmlAttributes) { }

        public AlertBuilder(string message, AlertType type, bool closable, object htmlAttributes)
            : base(HtmlTags.Div)
        {
            messageSequence = new List<string>();
            isClosable = closable;
            ControlAttributes(htmlAttributes);
            Message(message);
            Type(type);
        }

        public IAlertBuilder Closeable()
        {
            isClosable = true;
            return this;
        }

        public IAlertBuilder Type(AlertType type)
        {
            messageType = type;
            return this;
        }

        public IAlertBuilder Message(params string[] messages)
        {
            messageSequence.AddRange(messages);
            return this;
        }

        public string ToHtmlString()
        {
            if (isClosable)
            {
                HtmlTag.Append(HtmlTags.Button.Class("close")
                                              .Data("dismiss", "alert").Append("&times;"));
            }
            InitAttributes();
            HtmlTag.Append(string.Join(HtmlTags.Br.ToString(), messageSequence.Where(m => m.HasValue())));
            HtmlTag.Class(messageType.GetClass());
            return HtmlTag.ToString();
        }
    }
}