﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;
    using Helpers;

    public class DatePickerBuilder<TModel> : BaseInputBuilder<IDatePickerBuilder, TModel>, IDatePickerBuilder
    {
        private DateTime? date;
        private readonly string containerId;
        private string dateFormat = "MM/DD/YYYY";
        private string language = "en";

        public DatePickerBuilder(HtmlHelper<TModel> html, string name) : this(html, name, null) { }

        public DatePickerBuilder(HtmlHelper<TModel> html, string name, DateTime? date, object htmlAttributes = null)
            : base(html, HtmlTags.Input.Text.Name(name))
        {
            this.date = date;
            containerId = string.Concat("container", name);
            ControlAttributes(htmlAttributes);
        }

        public override IDatePickerBuilder Value(object value)
        {
            date = value as DateTime?;
            return this;
        }

        public IDatePickerBuilder Format(string format)
        {
            if (format.HasValue())
            {
                dateFormat = format;
            }
            return this;
        }

        public IDatePickerBuilder Language(string lang)
        {
            language = lang;
            return this;
        }

        public IDatePickerBuilder Width(int witdh, Unit unit = Unit.Pixels)
        {
            HtmlTag.Width(witdh + unit.GetDescription());
            return this;
        }

        public IDatePickerBuilder AsTimePicker()
        {
            dateFormat = "LT";
            return this;
        }

        public string ToHtmlString()
        {
            if (date.HasValue)
            {
                HtmlTag.Value(FormatHelper.MomentJsDateTimeFormat(date.Value, dateFormat));
            }
            HtmlTag.Class("form-control");
            InitAttributes();
            Html.RegisterScript(JavaScriptHelper.RegisterDatepicker(containerId, language, dateFormat));
            var inputGroupTag = HtmlTags.Div.Id(containerId)
                                            .Class("input-group date")
                                            .Append(HtmlTag)
                                            .Append(HtmlTags.Span.Class("input-group-addon")
                                                                    .Append(HtmlTags.I.Class("fa fa-calendar-o")));
            return inputGroupTag.ToString();
        }
    }
}