﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core.Base
{
    using Extensions;

    public abstract class BaseBuilder<TBuilder> where TBuilder : class
    {
        protected readonly HtmlTag HtmlTag;
        protected IDictionary<string, object> Attributes;

        protected BaseBuilder(HtmlTag rootTag)
        {
            HtmlTag = rootTag;
            Attributes = new Dictionary<string, object>();
        }

        public HtmlTag Tag { get { return HtmlTag; } }

        public virtual TBuilder Id(string id)
        {
            Attributes["id"] = id;
            return this as TBuilder;
        }

        public virtual TBuilder Class(string @class)
        {
            Attributes["class"] = @class;
            return this as TBuilder;
        }

        public TBuilder ControlAttributes(object htmlAttributes)
        {
            var values = htmlAttributes as Dictionary<string, object>;
            if (values != null)
            {
                Attributes = values;
                return this as TBuilder;
            }
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (attributes != null)
            {
                foreach (var attribute in attributes)
                {
                    if (attribute.Key.IsClassAttribute() && Attributes.ContainsKey(attribute.Key))
                    {
                        Attributes[attribute.Key] = Attributes[attribute.Key] + " " + attribute.Value;
                    }
                    else
                    {
                        Attributes[attribute.Key] = attribute.Value;
                    }
                }
            }
            return this as TBuilder;
        }

        public void InitAttributes()
        {
            foreach (var attribute in Attributes)
            {
                if (attribute.Key.IsClassAttribute())
                {
                    HtmlTag.Class(attribute.Value.ToString());
                }
                else
                {
                    HtmlTag.Add(attribute.Key, attribute.Value.ToString());
                }
            }
        }
    }
}
