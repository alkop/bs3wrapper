﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core.Base
{
    using Extensions;

    public abstract class BaseInputBuilder<TBuilder, TModel> : BaseBuilder<TBuilder> where TBuilder : class
    {
        protected readonly HtmlHelper<TModel> Html;

        protected BaseInputBuilder(HtmlHelper<TModel> html, HtmlTag rootTag)
            : base(rootTag)
        {
            Html = html;
        }

        public virtual TBuilder Value(object value)
        {
            HtmlTag.Value(value.ToStringWithNullCheck());
            return this as TBuilder;
        }

        public virtual TBuilder Name(string name)
        {
            HtmlTag.Name(name);
            return this as TBuilder;
        }

        public virtual TBuilder Readonly(bool isReadonly = true)
        {
            HtmlTag.Readonly(isReadonly);
            return this as TBuilder;
        }

        public virtual TBuilder Multiple()
        {
            HtmlTag.Add("multiple", "");
            return this as TBuilder;
        }

        public virtual TBuilder Disabled(bool isDisabled = true)
        {
            HtmlTag.Disabled(isDisabled);
            return this as TBuilder;
        }

        public virtual TBuilder Autofocus()
        {
            Attributes["autofocus"] = string.Empty;
            return this as TBuilder;
        }
    }
}