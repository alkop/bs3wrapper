﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core.Base
{
    using Enums;
    using Extensions;

    public abstract class BaseTextBoxBuilder<TBuilder, TModel> : BaseInputBuilder<TBuilder, TModel> where TBuilder : class
    {
        protected InputSize InputSize = InputSize.Default;

        protected BaseTextBoxBuilder(HtmlHelper<TModel> html, string name) : base(html, HtmlTags.Input.Text.Name(name)) { }

        protected BaseTextBoxBuilder(HtmlHelper<TModel> html, string name, object value, object htmlAttributes = null)
            : base(html, HtmlTags.Input.Text.Name(name).Value(value.ToStringWithNullCheck()))
        {
            ControlAttributes(htmlAttributes);
        }

        protected BaseTextBoxBuilder(HtmlHelper<TModel> html, HtmlTag tag) : base(html, tag) { }

        public TBuilder TabIndex(int index)
        {
            Attributes["tabindex"] = index;
            return this as TBuilder;
        }

        public TBuilder Size(InputSize inputSize)
        {
            InputSize = inputSize;
            return this as TBuilder;
        }

        public TBuilder Width(int witdh, Unit unit = Unit.Pixels)
        {
            HtmlTag.Width(witdh + unit.GetDescription());
            return this as TBuilder;
        }

        public TBuilder Autocomplete(Autocomplete type)
        {
            Attributes["autocomplete"] = type.GetDescription();
            return this as TBuilder;
        }

        public TBuilder MaxLength(int length)
        {
            Attributes["maxlength"] = length;
            return this as TBuilder;
        }

        public TBuilder Placeholder(string text)
        {
            HtmlTag.Attribute("placeholder", text);
            return this as TBuilder;
        }
    }
}
