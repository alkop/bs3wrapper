﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;

    public class CheckBoxBuilder<TModel> : BaseInputBuilder<ICheckBoxBuilder, TModel>, ICheckBoxBuilder
    {
        private string label;

        public CheckBoxBuilder(HtmlHelper<TModel> html, string name) : this(html, name, false) { }

        public CheckBoxBuilder(HtmlHelper<TModel> html, string name, bool isChecked, object htmlAttributes = null)
            : base(html, HtmlTags.Input.CheckBox.Name(name).Checked(isChecked))
        {
            ControlAttributes(htmlAttributes);
        }

        public ICheckBoxBuilder Checked(bool isChecked)
        {
            HtmlTag.Checked(isChecked);
            return this;
        }

        public ICheckBoxBuilder Label(string rightLabel)
        {
            label = rightLabel;
            return this;
        }

        public string ToHtmlString()
        {
            InitAttributes();
            return HtmlTags.Div.Class("checkbox")
                               .Append(HtmlTags.Label.Append(HtmlTag)
                                                     .Append(HtmlTags.Span.Class("cr")
                                                                          .Append(HtmlTags.I.Class("cr-icon glyphicon glyphicon-ok")))
                                                     .Append(label ?? string.Empty)).ToString();
        }
    }
}