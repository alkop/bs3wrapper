﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Linq;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;
    using Helpers;

    public class ButtonBuilder<TModel> : BaseBuilder<IButtonBuilder>, IButtonBuilder
    {
        private readonly HtmlHelper<TModel> html;
        private IFontIconBuilder icon;
        private IBadgeBuilder badge;
        private ButtonType type;
        private ButtonSize size;
        private bool isSubmit, isActive, isDisabled;
        private string text, id;

        public ButtonBuilder(HtmlHelper<TModel> html) : this(html, string.Empty) { }

        public ButtonBuilder(HtmlHelper<TModel> html, string label) : this(html, label, string.Empty) { }

        public ButtonBuilder(HtmlHelper<TModel> html, string label, string fontIcon) : this(html, label, fontIcon, ButtonType.Default) { }

        public ButtonBuilder(HtmlHelper<TModel> html, string label, ButtonType buttonType) : this(html, label, string.Empty, buttonType) { }

        public ButtonBuilder(HtmlHelper<TModel> html, string label, string fontIcon, ButtonType buttonType)
            : base(HtmlTags.Button)
        {
            this.html = html;
            Label(label);
            Icon(fontIcon);
            Type(buttonType);
        }

        public IButtonBuilder Label(string label)
        {
            text = label;
            return this;
        }

        public IButtonBuilder Submit()
        {
            isSubmit = true;
            return this;
        }

        public IButtonBuilder Type(ButtonType buttonType)
        {
            type = buttonType;
            return this;
        }

        public IButtonBuilder Size(ButtonSize buttonSize)
        {
            size = buttonSize;
            return this;
        }

        public IButtonBuilder TriggerModal(string modalId)
        {
            HtmlTag.Data("target", string.Concat("#", modalId))
                   .Data("toggle", "modal");
            return this;
        }

        public IButtonBuilder TriggerValidation(string buttonId, string formId)
        {
            id = buttonId;
            html.RegisterScript(JavaScriptHelper.RegisterFormValidation(buttonId, "#" + formId));
            return this;
        }

        public IButtonBuilder TriggerValidation(string buttonId)
        {
            id = buttonId;
            html.RegisterScript(JavaScriptHelper.RegisterFormValidation(buttonId));
            return this;
        }

        public IButtonBuilder Tooltip(string message, Placement placement = Placement.Top, params TriggerEvent[] trigger)
        {
            HtmlTag.Attribute("title", message);
            HtmlTag.Data("toggle", "tooltip");
            HtmlTag.Data("placement", placement.GetDescription());
            trigger = trigger.Any() ? trigger : new[] { TriggerEvent.Hover, TriggerEvent.Focus };
            HtmlTag.Data("trigger", string.Join(" ", trigger.Select(t => t.GetDescription())));
            return this;
        }

        public IButtonBuilder Popover(string message, string title = "", Placement placement = Placement.Top, params TriggerEvent[] trigger)
        {
            HtmlTag.Data("content", message);
            HtmlTag.Data("title", title);
            HtmlTag.Data("toggle", "popover");
            HtmlTag.Data("container", "body");
            HtmlTag.Data("placement", placement.GetDescription());
            trigger = trigger.Any() ? trigger : new[] { TriggerEvent.Click };
            HtmlTag.Data("trigger", string.Join(" ", trigger.Select(t => t.GetDescription())));
            return this;
        }

        public IButtonBuilder Icon(IFontIconBuilder fontIcon)
        {
            icon = fontIcon;
            return this;
        }

        public IButtonBuilder Icon(string fontIcon)
        {
            icon = new FontIconBuilder(fontIcon);
            return this;
        }

        public IButtonBuilder Badge(IBadgeBuilder badgeBuilder)
        {
            badge = badgeBuilder;
            return this;
        }

        public IButtonBuilder Badge(string badgeText)
        {
            badge = new BadgeBuilder(badgeText);
            return this;
        }

        public IButtonBuilder Active()
        {
            isActive = true;
            return this;
        }

        public IButtonBuilder Disabled()
        {
            isDisabled = true;
            return this;
        }

        public string ToHtmlString()
        {
            HtmlTag.Attribute("type", isSubmit ? "submit" : "button")
                   .Class(type.GetClass())
                   .Class(size.GetClass(), size != ButtonSize.Default)
                   .Class("active", isActive)
                   .NotEmptyId(id);

            if (isDisabled)
            {
                Attributes["disabled"] = "disabled";
            }

            InitAttributes();

            if (icon != null)
            {
                HtmlTag.Append(icon.ToHtmlString());
                HtmlTag.Append(Constants.Nbsp);
            }

            HtmlTag.Append(text);

            if (badge != null)
            {
                HtmlTag.Append(Constants.Nbsp);
                HtmlTag.Append(badge.ToHtmlString());
            }

            return HtmlTag.ToString();
        }
    }
}