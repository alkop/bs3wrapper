/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;

    public class LabelBuilder : BaseBuilder<ILabelBuilder>, ILabelBuilder
    {
        private string text;
        private LabelType type;

        public LabelBuilder() : this(string.Empty) { }

        public LabelBuilder(string text)
            : base(HtmlTags.Span)
        {
            this.text = text;
        }

        public ILabelBuilder Type(LabelType labelType)
        {
            type = labelType;
            return this;
        }

        public ILabelBuilder Text(string labelText)
        {
            text = labelText;
            return this;
        }

        public string ToHtmlString()
        {
            InitAttributes();
            HtmlTag.Class(type.GetClass());
            HtmlTag.Append(text);
            return HtmlTag.ToString();
        }
    }
}