﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Linq;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Extensions;
    using Models;

    public class BreadcrumbBuilder : BaseBuilder<IBreadcrumbBuilder>, IBreadcrumbBuilder
    {
        private readonly List<BreadcrumbLinkViewModel> links;

        public BreadcrumbBuilder(List<BreadcrumbLinkViewModel> links = null) : base(HtmlTags.Ol.Class("breadcrumb"))
        {
            this.links = links ?? new List<BreadcrumbLinkViewModel>();
        }

        public IBreadcrumbBuilder Add(string name, string url)
        {
            links.Add(new BreadcrumbLinkViewModel(name, url));
            return this;
        }

        public IBreadcrumbBuilder Remove(string url)
        {
            links.RemoveAll(t => t.Url == url);
            return this;
        }

        public string ToHtmlString()
        {
            foreach (var link in links)
            {
                bool isLast = link.Equals(links.Last());
                var liTag = HtmlTags.Li.NotEmptyClass(isLast ? "active" : string.Empty);
                if (isLast)
                {
                    liTag.Append(link.Name.EmptyIfNull());
                }
                else
                {
                    liTag.Append(HtmlTags.A.Href(link.Url ?? "#").Append(link.Name.EmptyIfNull()));
                }
                HtmlTag.Append(liTag);
            }
            return HtmlTag.ToString();
        }
    }
}