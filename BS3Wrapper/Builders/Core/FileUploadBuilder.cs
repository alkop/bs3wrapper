﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Linq;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;
    using Helpers;

    public class FileUploadBuilder<TModel> : BaseInputBuilder<IFileUploadBuilder, TModel>, IFileUploadBuilder
    {
        private readonly HtmlHelper<TModel> html;
        private Width[] ctrlWidth;
        private string chooseFileLabel = "Browse";
        private string changeFileLabel = "Change";
        private string clearLabel = "Clear";

        public FileUploadBuilder(HtmlHelper<TModel> html, string name) : base(html, HtmlTags.Input.File.Name(name))
        {
            this.html = html;
        }

        public FileUploadBuilder(HtmlHelper<TModel> html, string name, object value, object htmlAttributes = null)
            : base(html, HtmlTags.Input.File.Name(name).Value(value.ToStringWithNullCheck()))
        {
            this.html = html;
            ControlAttributes(htmlAttributes);
        }

        public IFileUploadBuilder Width(params Width[] width)
        {
            ctrlWidth = width;
            return this;
        }

        public IFileUploadBuilder ChooseFileLabel(string label)
        {
            chooseFileLabel = label;
            return this;
        }

        public IFileUploadBuilder ChangeFileLabel(string label)
        {
            changeFileLabel = label;
            return this;
        }

        public IFileUploadBuilder ClearLabel(string label)
        {
            clearLabel = label;
            return this;
        }

        public string ToHtmlString()
        {
            InitAttributes();
            html.RegisterScript(JavaScriptHelper.RegisterFileUpload());
            var controlTag = HtmlTags.Div.Class("fileinput fileinput-new input-group")
                                     .Data("provides", "fileinput")
                                     .Append(HtmlTags.Div.Class("form-control")
                                                         .Data("trigger", "fileinput")
                                                         .Append(HtmlTags.I.Class("fa fa-file fileinput-exists"))
                                                         .Append(HtmlTags.Span.Class("fileinput-filename")))
                                     .Append(HtmlTags.Span.Class("input-group-addon btn btn-default btn-file")
                                                          .Append(HtmlTags.Span.Class("fileinput-new").Append(chooseFileLabel))
                                                          .Append(HtmlTags.Span.Class("fileinput-exists").Append(changeFileLabel))
                                                          .Append(HtmlTag))
                                     .Append(HtmlTags.A.Href("javascript:void(0)")
                                                       .Class("input-group-addon btn btn-default fileinput-exists")
                                                       .Data("dismiss", "fileinput")
                                                       .Append(clearLabel));
            if (ctrlWidth != null)
            {
                foreach (var width in ctrlWidth.Where(width => width != Enums.Width.None))
                {
                    controlTag.Class(width.GetClass());
                }
            }
            return controlTag.ToString();
        }
    }
}