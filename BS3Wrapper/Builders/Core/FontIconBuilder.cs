﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Linq;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;
    using Enums;
    using Extensions;

    public class FontIconBuilder : BaseBuilder<IFontIconBuilder>, IFontIconBuilder
    {
        public FontIconBuilder(string icon) : base(HtmlTags.I.Class(icon)) { }

        public IFontIconBuilder Tooltip(string message, Placement placement = Placement.Top, params TriggerEvent[] trigger)
        {
            HtmlTag.Attribute("title", message);
            HtmlTag.Data("toggle", "tooltip");
            HtmlTag.Data("placement", placement.GetDescription());
            trigger = trigger.Any() ?  trigger : new[] { TriggerEvent.Hover, TriggerEvent.Focus };
            HtmlTag.Data("trigger", string.Join(" ", trigger.Select(t => t.GetDescription())));
            return this;
        }

        public IFontIconBuilder Popover(string message, string title = "", Placement placement = Placement.Top, params TriggerEvent[] trigger)
        {
            HtmlTag.Data("content", message);
            HtmlTag.Data("title", title);
            HtmlTag.Data("toggle", "popover");
            HtmlTag.Data("container", "body");
            HtmlTag.Data("placement", placement.GetDescription());
            trigger = trigger.Any() ? trigger : new[] { TriggerEvent.Click };
            HtmlTag.Data("trigger", string.Join(" ", trigger.Select(t => t.GetDescription())));
            return this;
        }

        public IFontIconBuilder Size(int size, Unit unit = Unit.Pixels)
        {
            HtmlTag.Style("font-size", size + unit.GetDescription());
            return this;
        }

        public IFontIconBuilder Color(string color)
        {
            HtmlTag.Style("color", color);
            return this;
        }

        public string ToHtmlString()
        {
            InitAttributes();
            return HtmlTag.ToString();
        }
    }
}