﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Builders.Core.Contracts
{
    using Base.Input;

    public interface ITypeaheadBuilder : IBaseTextBoxBuilder<ITypeaheadBuilder>
    {
        /// <summary>
        /// The data source to query against. Its URL to action that return JSON.
        /// </summary>
        ITypeaheadBuilder Source(string url);
        /// <summary>
        /// Adds a delay between lookups.
        /// </summary>
        ITypeaheadBuilder Delay(int delay);
        /// <summary>
        /// The max number of items to display in the dropdown.
        /// </summary>
        ITypeaheadBuilder Items(int items);
        /// <summary>
        /// If hints should be shown when applicable as soon as the input gets focus.
        /// </summary>
        ITypeaheadBuilder ShowHintOnFocus();
        /// <summary>
        /// The method used to determine if a query matches an item. Accepts a single argument, the item against which to test the query. Access the current query with this.query. Return a boolean true if query is a match.
        /// </summary>
        ITypeaheadBuilder Matcher(string function);
        /// <summary>
        /// Method used to sort autocomplete results. Accepts a single argument items and has the scope of the typeahead instance. Reference the current query with this.query.
        /// </summary>
        ITypeaheadBuilder Sorter(string function);
        /// <summary>
        /// The method used to return selected item. Accepts a single argument, the item and has the scope of the typeahead instance.
        /// </summary>
        ITypeaheadBuilder Updater(string function);
        /// <summary>
        /// Method used to highlight autocomplete results. Accepts a single argument item and has the scope of the typeahead instance. Should return html.
        /// </summary>
        ITypeaheadBuilder Highlighter(string function);
        /// <summary>
        /// Method used to get textual representation of an item of the sources. Accepts a single argument item and has the scope of the typeahead instance. Should return a String.
        /// </summary>
        ITypeaheadBuilder DisplayText(string function);
    }
}