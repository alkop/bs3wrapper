﻿using System;

namespace BS3Wrapper.Builders.Core.Contracts
{
    public interface IJumbotronBuilder : IDisposable { }
}