/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Builders.Core.Contracts.Base.Input
{
    using Enums;

    public interface IBaseTextBoxBuilder<out TBuilder> : IBaseInputBuilder<TBuilder>
    {
        /// <summary>
        /// Enable / Disable autocomplete for input
        /// </summary>
        TBuilder Autocomplete(Autocomplete type);
        /// <summary>
        /// Make input readonly
        /// </summary>
        TBuilder MaxLength(int length);
        /// <summary>
        /// Set short hint that describes the expected value of input
        /// </summary>
        TBuilder Placeholder(string text);
        /// <summary>
        /// Set order of the transition between the elements by pressing TAB.
        /// </summary>
        TBuilder TabIndex(int index);
        /// <summary>
        /// Change input size
        /// </summary>
        TBuilder Size(InputSize inputSize);
    }
}