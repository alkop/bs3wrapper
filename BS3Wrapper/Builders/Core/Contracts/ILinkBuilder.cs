﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Builders.Core.Contracts
{
    using Base;
    using Enums;

    public interface ILinkBuilder : IBaseBuilder<ILinkBuilder>
    {
        /// <summary>
        /// Set href for link
        /// </summary>
        ILinkBuilder Action(string link);
        /// <summary>
        /// Set label (caption) for link
        /// </summary>
        ILinkBuilder Label(string label);
        /// <summary>
        /// Change link type
        /// </summary>
        ILinkBuilder Type(ButtonType buttonType);
        /// <summary>
        /// Change link size
        /// </summary>
        ILinkBuilder Size(ButtonSize buttonSize);
        /// <summary>
        /// Set link as modal window toggle
        /// </summary>
        ILinkBuilder TriggerModal(string modalId);
        /// <summary>
        /// Show tooltip
        /// </summary>
        ILinkBuilder Tooltip(string message, Placement placement = Placement.Top, params TriggerEvent[] trigger);
        /// <summary>
        /// Show popover
        /// </summary>
        ILinkBuilder Popover(string message, string title = "", Placement placement = Placement.Top, params TriggerEvent[] trigger);
        /// <summary>
        /// Show icon in button
        /// </summary>
        ILinkBuilder Icon(IFontIconBuilder fontIcon);
        /// <summary>
        /// Show icon in button
        /// </summary>
        ILinkBuilder Icon(string fontIcon);
        /// <summary>
        /// Set badge for button
        /// </summary>
        ILinkBuilder Badge(IBadgeBuilder badge);
        /// <summary>
        /// Set badge for button
        /// </summary>
        ILinkBuilder Badge(string text);
        /// <summary>
        /// Link will appear pressed (with a darker background, darker border, and inset shadow) when active
        /// </summary>
        ILinkBuilder Active();
        /// <summary>
        /// Make link disabled
        /// </summary>
        ILinkBuilder Disabled();
    }
}