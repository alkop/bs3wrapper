﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Builders.Core.Contracts
{
    using Base;
    using Enums;

    public interface IButtonBuilder : IBaseBuilder<IButtonBuilder>
    {
        /// <summary>
        /// Set label (caption) for button
        /// </summary>
        IButtonBuilder Label(string label);
        /// <summary>
        /// Set button type as "submit"
        /// </summary>
        IButtonBuilder Submit();
        /// <summary>
        /// Change button type
        /// </summary>
        IButtonBuilder Type(ButtonType buttonType);
        /// <summary>
        /// Change button size
        /// </summary>
        IButtonBuilder Size(ButtonSize buttonSize);
        /// <summary>
        /// Set button as modal window toggle
        /// </summary>
        IButtonBuilder TriggerModal(string modalId);
        /// <summary>
        /// Run form validation on form
        /// </summary>
        IButtonBuilder TriggerValidation(string buttonId, string formId);
        /// <summary>
        /// Run form validation on form
        /// </summary>
        IButtonBuilder TriggerValidation(string buttonId);
        /// <summary>
        /// Show tooltip
        /// </summary>
        IButtonBuilder Tooltip(string message, Placement placement = Placement.Top, params TriggerEvent[] trigger);
        /// <summary>
        /// Show popover
        /// </summary>
        IButtonBuilder Popover(string message, string title = "", Placement placement = Placement.Top, params TriggerEvent[] trigger);
        /// <summary>
        /// Show icon in button
        /// </summary>
        IButtonBuilder Icon(IFontIconBuilder fontIcon);
        /// <summary>
        /// Show icon in button
        /// </summary>
        IButtonBuilder Icon(string fontIcon);
        /// <summary>
        /// Set badge for button
        /// </summary>
        IButtonBuilder Badge(IBadgeBuilder badge);
        /// <summary>
        /// Set badge for button
        /// </summary>
        IButtonBuilder Badge(string text);
        /// <summary>
        /// Button will appear pressed (with a darker background, darker border, and inset shadow) when active
        /// </summary>
        IButtonBuilder Active();
        /// <summary>
        /// Make button disabled
        /// </summary>
        IButtonBuilder Disabled();
    }
}