﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BS3Wrapper.Builders.Core
{
    using Base;
    using Contracts;

    public class SelectBuilder<TModel> : BaseInputBuilder<ISelectBuilder, TModel>, ISelectBuilder
    {
        private readonly string value;
        private readonly IEnumerable<SelectListItem> options;
        private string emptyOption;


        public SelectBuilder(HtmlHelper<TModel> html, string name, IEnumerable<SelectListItem> options)
            : base(html, HtmlTags.Select.Name(name))
        {
            this.options = options;
        }

        public SelectBuilder(HtmlHelper<TModel> html, string id, string name, IEnumerable<SelectListItem> options)
            : base(html, HtmlTags.Select.Name(name).Id(id))
        {
            this.options = options;
        }

        public SelectBuilder(HtmlHelper<TModel> html, string id, string name, string value, IEnumerable<SelectListItem> options)
            : base(html, HtmlTags.Select.Name(name).Id(id))
        {
            this.value = value;
            this.options = options;
        }

        public ISelectBuilder ShowEmptyOption(string label = Constants.DropDownEmptyText)
        {
            emptyOption = label;
            return this;
        }

        public ISelectBuilder Multiple()
        {
            Attributes["multiple"] = string.Empty;
            return this;
        }

        public string ToHtmlString()
        {
            InitAttributes();
            HtmlTag.Class("form-control");
            if (emptyOption != null)
            {
                HtmlTag.Append(HtmlTags.Option.Append(emptyOption).Value(string.Empty));
            }
            foreach (var option in options)
            {
                HtmlTag.Append(HtmlTags.Option.Selected(option.Selected || value == option.Value)
                                              .Value(option.Value)
                                              .Append(option.Text));
            }
            return HtmlTag.ToString();
        }
    }
}