﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Enums
{
    using Attributes;

    public enum Width
    {
        None,

        #region Extra small devices Phones (<768px)
        [Class("col-xs-1")]
        ColXs1,
        [Class("col-xs-2")]
        ColXs2,
        [Class("col-xs-3")]
        ColXs3,
        [Class("col-xs-4")]
        ColXs4,
        [Class("col-xs-5")]
        ColXs5,
        [Class("col-xs-6")]
        ColXs6,
        [Class("col-xs-7")]
        ColXs7,
        [Class("col-xs-8")]
        ColXs8,
        [Class("col-xs-9")]
        ColXs9,
        [Class("col-xs-10")]
        ColXs10,
        [Class("col-xs-11")]
        ColXs11,
        [Class("col-xs-12")]
        ColXs12,
        #endregion

        #region Small devices Tablets (≥768px)
        [Class("col-sm-1")]
        ColSm1,
        [Class("col-sm-2")]
        ColSm2,
        [Class("col-sm-3")]
        ColSm3,
        [Class("col-sm-4")]
        ColSm4,
        [Class("col-sm-5")]
        ColSm5,
        [Class("col-sm-6")]
        ColSm6,
        [Class("col-sm-7")]
        ColSm7,
        [Class("col-sm-8")]
        ColSm8,
        [Class("col-sm-9")]
        ColSm9,
        [Class("col-sm-10")]
        ColSm10,
        [Class("col-sm-11")]
        ColSm11,
        [Class("col-sm-12")]
        ColSm12,
        #endregion

        #region Medium devices Desktops (≥992px)
        [Class("col-md-1")]
        ColMd1,
        [Class("col-md-2")]
        ColMd2,
        [Class("col-md-3")]
        ColMd3,
        [Class("col-md-4")]
        ColMd4,
        [Class("col-md-5")]
        ColMd5,
        [Class("col-md-6")]
        ColMd6,
        [Class("col-md-7")]
        ColMd7,
        [Class("col-md-8")]
        ColMd8,
        [Class("col-md-9")]
        ColMd9,
        [Class("col-md-10")]
        ColMd10,
        [Class("col-md-11")]
        ColMd11,
        [Class("col-md-12")]
        ColMd12,
        #endregion

        #region Large devices Desktops (≥1200px)
        [Class("col-lg-1")]
        ColLg1,
        [Class("col-lg-2")]
        ColLg2,
        [Class("col-lg-3")]
        ColLg3,
        [Class("col-lg-4")]
        ColLg4,
        [Class("col-lg-5")]
        ColLg5,
        [Class("col-lg-6")]
        ColLg6,
        [Class("col-lg-7")]
        ColLg7,
        [Class("col-lg-8")]
        ColLg8,
        [Class("col-lg-9")]
        ColLg9,
        [Class("col-lg-10")]
        ColLg10,
        [Class("col-lg-11")]
        ColLg11,
        [Class("col-lg-12")]
        ColLg12
        #endregion
    }
}
