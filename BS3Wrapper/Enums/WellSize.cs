﻿namespace BS3Wrapper.Enums
{
    using Attributes;

    public enum WellSize
    {
        [Class("")]
        Default,
        [Class("well-lg")]
        Large,
        [Class("well-sm")]
        Small
    }
}
