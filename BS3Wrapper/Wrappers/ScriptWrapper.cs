﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BS3Wrapper.Wrappers
{
    using Helpers;

    public partial class BS3<TModel>
    {
        public IHtmlString RenderScripts()
        {
            var ctx = html.ViewContext.HttpContext;

            var scripts = DefaultScripts();
            var controlScripts = ctx.Items[Constants.ScriptsKey] as Stack<string>;
            if (controlScripts != null)
            {
                scripts.AddRange(controlScripts);
            }
            var scriptTag = HtmlTags.Script.Attribute("type", "text/javascript");
            foreach (var script in scripts)
            {
                scriptTag.Append("\n");
                scriptTag.Append(script);
            }
            return scriptTag.ToHtml();
        }

        private List<string> DefaultScripts()
        {
            var routeData = html.ViewContext.RouteData.Values;
            return new[]
            {
                JavaScriptHelper.Init(routeData["controller"].ToString(), routeData["action"].ToString(), string.Empty, "/")

            }.ToList();
        }
    }
}
