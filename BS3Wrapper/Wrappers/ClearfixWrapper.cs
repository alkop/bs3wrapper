﻿
using HtmlBuilders;
using System.Web;

namespace BS3Wrapper.Wrappers
{
    public partial class BS3<TModel>
    {
        public IHtmlString ClearFix()
        {
            return HtmlTags.Div.Class("clearfix").ToHtml();
        }
    }
}
