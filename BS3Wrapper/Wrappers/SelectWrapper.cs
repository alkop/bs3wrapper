﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BS3Wrapper.Wrappers
{
    using Builders.Core;
    using Builders.Core.Contracts;
    using Builders.Group;
    using Builders.Group.Contracts;

    public partial class BS3<TModel>
    {
        public ISelectBuilder Select(string name, IEnumerable<SelectListItem> selectList)
        {
            return new SelectBuilder<TModel>(html, name, selectList);
        }

        public ISelectBuilder Select(string id, string name, IEnumerable<SelectListItem> selectList)
        {
            return new SelectBuilder<TModel>(html, id, name, selectList);
        }

        public ISelectBuilder Select(string id, string name, string value, IEnumerable<SelectListItem> selectList)
        {
            return new SelectBuilder<TModel>(html, id, name, value, selectList);
        }

        public ISelectBuilder SelectFor<TProperty>(Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList)
        {
            string id = html.IdFor(expression).ToHtmlString();
            string name = html.NameFor(expression).ToHtmlString();
            var meta = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            object value = meta.Model ?? string.Empty;
            return new SelectBuilder<TModel>(html, id, name, value.ToString(), selectList);
        }

        public ISelectGroupBuilder SelectGroupFor<TProperty>(Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList)
        {
            return new SelectGroupBuilder<TModel, TProperty>(html, expression, selectList);
        }
    }
}
