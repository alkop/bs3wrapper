﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BS3Wrapper.Wrappers
{
    using Builders.Core;
    using Builders.Core.Contracts;
    using Builders.Group;
    using Builders.Group.Contracts;

    public partial class BS3<TModel>
    {
        public ITypeaheadBuilder Typeahead(string ctrlTextName)
        {
            return new TypeaheadBuilder<TModel>(html, ctrlTextName, ctrlTextName);
        }

        public ITypeaheadBuilder Typeahead(string ctrlTextId, string ctrlTextName)
        {
            return new TypeaheadBuilder<TModel>(html, ctrlTextId, ctrlTextName);
        }

        public ITypeaheadBuilder Typeahead(string ctrlTextId, string ctrlTextName, object ctrlTextValue)
        {
            return new TypeaheadBuilder<TModel>(html, ctrlTextId, ctrlTextName, ctrlTextValue);
        }

        public ITypeaheadBuilder Typeahead(string ctrlKeyName, string ctrlTextName, object ctrlKeyValue, object ctrlTextValue, object htmlAttributes = null)
        {
            return new TypeaheadBuilder<TModel>(html, ctrlKeyName, ctrlTextName, ctrlKeyName, ctrlTextName, ctrlKeyValue, ctrlTextValue, htmlAttributes);
        }

        public ITypeaheadBuilder Typeahead(string ctrlKeyId, string ctrlTextId, string ctrlKeyName, string ctrlTextName, object ctrlKeyValue, object ctrlTextValue, object htmlAttributes = null)
        {
            return new TypeaheadBuilder<TModel>(html, ctrlKeyId, ctrlTextId, ctrlKeyName, ctrlTextName, ctrlKeyValue, ctrlTextValue, htmlAttributes);
        }

        public ITypeaheadBuilder TypeaheadFor<TProperty>(Expression<Func<TModel, TProperty>> ctrlTextExpression, object htmlAttributes = null)
        {
            var ctrlTextMeta = ModelMetadata.FromLambdaExpression(ctrlTextExpression, html.ViewData);
            return Typeahead(null, html.IdFor(ctrlTextExpression).ToString(), null, html.NameFor(ctrlTextExpression).ToString(), null, ctrlTextMeta.Model, htmlAttributes);
        }

        public ITypeaheadBuilder TypeaheadFor<TKeyProperty, TTextProperty>(Expression<Func<TModel, TKeyProperty>> ctrlKeyExpression, Expression<Func<TModel, TTextProperty>> ctrlTextExpression, object htmlAttributes = null)
        {
            var ctrlKeyMeta = ModelMetadata.FromLambdaExpression(ctrlKeyExpression, html.ViewData);
            var ctrlTextMeta = ModelMetadata.FromLambdaExpression(ctrlTextExpression, html.ViewData);
            return Typeahead(html.IdFor(ctrlKeyExpression).ToString(), html.IdFor(ctrlTextExpression).ToString(),
                             html.NameFor(ctrlKeyExpression).ToString(), html.NameFor(ctrlTextExpression).ToString(),
                             ctrlKeyMeta.Model, ctrlTextMeta.Model, htmlAttributes);
        }

        public ITypeaheadGroupBuilder TypeaheadGroupFor<TTextProperty>(Expression<Func<TModel, TTextProperty>> expression)
        {
            return new TypeaheadGroupBuilder<TModel, TTextProperty, TTextProperty>(html, expression);
        }

        public ITypeaheadGroupBuilder TypeaheadGroupFor<TKeyProperty, TTextProperty>(Expression<Func<TModel, TKeyProperty>> ctrlKeyExpression, Expression<Func<TModel, TTextProperty>> ctrlTextExpression)
        {
            return new TypeaheadGroupBuilder<TModel, TKeyProperty, TTextProperty>(html, ctrlKeyExpression, ctrlTextExpression);
        }
    }
}
