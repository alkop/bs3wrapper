﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Wrappers
{
    using Builders.Complex;
    using Builders.Complex.Contracts;
    using Enums;

    public partial class BS3<TModel>
    {
        public IButtonGroupBuilder ButtonGroup()
        {
            return new ButtonGroupBuilder<TModel>(html);
        }

        public IButtonGroupBuilder ButtonGroup(bool vertical)
        {
            return new ButtonGroupBuilder<TModel>(html, vertical);
        }

        public IButtonGroupBuilder ButtonGroup(bool justified, ButtonGroupSize size)
        {
            return new ButtonGroupBuilder<TModel>(html, false, justified, size);
        }

        public IButtonGroupBuilder ButtonGroup(bool vertical, ButtonGroupSize size, object htmlAttributes)
        {
            return new ButtonGroupBuilder<TModel>(html, vertical, false, size, htmlAttributes);
        }

        public IButtonGroupBuilder ButtonGroup(object htmlAttributes)
        {
            return new ButtonGroupBuilder<TModel>(html, htmlAttributes: htmlAttributes);
        }

        public IButtonGroupBuilder ButtonGroup(bool vertical, object htmlAttributes)
        {
            return new ButtonGroupBuilder<TModel>(html, vertical);
        }

        public IButtonGroupBuilder ButtonGroup(ButtonGroupSize size)
        {
            return new ButtonGroupBuilder<TModel>(html, size: size);
        }
    }
}
