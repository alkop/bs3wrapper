﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BS3Wrapper.Attributes.Validation
{
    using Base;
    using Extensions;

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DecimalAttribute : ValidationAttribute, IGroupClientValidation
    {
        private int precision = 18;
        private int scale = 4;

        [DefaultValue(18)]
        public int Precision
        {
            get { return precision; }
            set { precision = value; }
        }

        [DefaultValue(4)]
        public int Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        public DecimalAttribute() : base("Field {0} must be decimal.") { }

        public override bool IsValid(object value)
        {
            return value == null || Convert.ToString(value).IsDecimal(precision, scale);
        }

        public void InjectHtmlAttributes(HtmlTag group, string label)
        {
            group.Class("decimal")
                 .Data("precision", precision.ToString())
                 .Data("scale", scale.ToString())
                 .Data("decimal-message", FormatErrorMessage(label));
        }
    }
}