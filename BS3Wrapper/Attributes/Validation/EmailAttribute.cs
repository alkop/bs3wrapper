﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using HtmlBuilders;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace BS3Wrapper.Attributes.Validation
{
	using Base;

	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class EmailAttribute : ValidationAttribute, IGroupClientValidation
	{
		private static readonly Regex Regex = new Regex(@"^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

		public EmailAttribute() : base("Email address is not valid.") { }

		public override bool IsValid(object value)
		{
		    if (value == null)
		    {
                return true;
		    }
		    var emails = value.ToString().Split(',');
            return emails.Aggregate(true, (current, email) => current & Regex.Match(email.Trim()).Length > 0);
		}

		public void InjectHtmlAttributes(HtmlTag group, string label)
		{
			group.Class("email")
				 .Data("email-message", FormatErrorMessage(label));
		}
	}
}