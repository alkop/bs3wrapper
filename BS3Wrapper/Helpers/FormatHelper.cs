﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BS3Wrapper.Helpers
{
    using Extensions;

    public static class FormatHelper
    {
        private static readonly IDictionary<Type, Func<object, string, string>> FormatDictionary;
        private static readonly IDictionary<char, char> MomentJsToSharpFormatMapping;

        static FormatHelper()
        {
            FormatDictionary = new Dictionary<Type, Func<object, string, string>>
                {
                    {typeof(DateTime), DateTimeFormat},
                    {typeof(TimeSpan), TimeSpanFormat},
                };

            MomentJsToSharpFormatMapping = new Dictionary<char, char> { { 'Y', 'y' }, { 'D', 'd' }, { 'Z', 'z' } };
        }

        public static string Format(object value, string format = null)
        {
            string formated;
            Func<object, string, string> func;

            if (FormatDictionary.TryGetValue(value.GetType(), out func))
            {
                formated = func.Invoke(value, format);
            }
            else
            {
                formated = format.HasValue() ? format.FormatBy(value) : value.ToString();
            }
            return formated;
        }

        public static string DateTimeFormat(object value, string format = null)
        {
            if (value is DateTime)
            {
                return format.HasValue() ? ((DateTime)value).ToString(format, CultureInfo.InvariantCulture) : value.ToString();
            }
            return string.Empty;
        }

        public static string MomentJsDateTimeFormat(DateTime date, string format)
        {
            format = MomentJsToSharpFormatMapping.Aggregate(format, (current, map) => current.Replace(map.Key, map.Value));
            return date.ToString(format, CultureInfo.InvariantCulture);
        }

        public static string TimeSpanFormat(object value, string formatString = null)
        {
            if (value is TimeSpan)
            {
                return formatString.HasValue() ? ((TimeSpan)value).ToString(formatString, CultureInfo.InvariantCulture) : value.ToString();
            }
            return string.Empty;
        }
    }
}
