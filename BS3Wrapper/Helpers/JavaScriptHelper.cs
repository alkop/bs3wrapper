﻿/*
The MIT License (MIT)

Copyright (c) 2016 Alexander Kopylov, Sergey Ivashkevich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace BS3Wrapper.Helpers
{
    using Extensions;

    public static class JavaScriptHelper
    {
        public static string Init(string controller, string action, string param, string url)
        {
            return "BS3.Utils.init('{0}', '{1}', '{2}', '{3}');".FormatBy(controller, action, param, url);
        }

        public static string RegisterDatepicker(string id, string lang, string format)
        {
            return "BS3.Utils.registerDatepicker('#{0}', '{1}', '{2}');".FormatBy(id, lang, format);
        }

        public static string RegisterFileUpload()
        {
            return "BS3.Utils.registerFileUpload();";
        }

        public static string RegisterFormValidation(string id, string container = "form")
        {
            return "BS3.Utils.registerFormValidation('{0}', '{1}');".FormatBy(id, container);
        }

        public static string RegisterTextareaLimit(string message = null)
        {
            return "BS3.Utils.registerTextareaLimit('{0}');".FormatBy(message ?? string.Empty);
        }

        public static string EnableOpenDropDownMenuOnHover()
        {
            return "BS3.Utils.openNavBarDropDownMenuOnHover();";
        }

        public static string RegisterTypeahead(string ctrlKeyId, string ctrlTextId, string url, int items = 8, int delay = 0, bool showHintOnFocus = false,
                                               string functionMatcher = null, string functionSorter = null, string functionUpdater = null,
                                               string functionHighlighter = null, string functionDisplayText = null)
        {
            var undefined = "undefined";
            functionMatcher = functionMatcher.HasValue() ? functionMatcher : undefined;
            functionSorter = functionSorter.HasValue() ? functionSorter : undefined;
            functionUpdater = functionUpdater.HasValue() ? functionUpdater : undefined;
            functionHighlighter = functionHighlighter.HasValue() ? functionHighlighter : undefined;
            functionDisplayText = functionDisplayText.HasValue() ? functionDisplayText : undefined;
            return "BS3.Utils.registerTypeahead('{0}', '{1}', '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10});".FormatBy(ctrlKeyId, ctrlTextId, url, items, delay, showHintOnFocus.ToString().ToLower(),
                                                                                                                         functionMatcher, functionSorter, functionUpdater, functionHighlighter, functionDisplayText);
        }
    }
}
