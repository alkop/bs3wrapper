var BS3 = {
    activeController: 'home',
    activeAction: 'index',
    activeParam: '',
    siteUrl: '',
    Validate: {
        integer: function (value) {
            var r = new RegExp('^\\d{1,9}$');
            return r.test(value);
        },
        decimal: function (value, precision, scale) {
            var r = new RegExp('^\\d{1,' + (precision - scale) + '}(\\,\\d{1,' + scale + '})?$');
            return r.test(value);
        },
        email: function (value) {
            var r = new RegExp('^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$');
            return r.test(value);
        },
        time: function (value) {
            var r = new RegExp('^([0-9]|[01][0-9]|2[0-3]):([0-5][0-9])$');
            return r.test(value);
        },
        diff_time: function (value) {
            var r = new RegExp('^([0-9]{1,2})(:[0-5]\\d)$');
            return r.test(value);
        },
        date: function (value) {
            var r = new RegExp('^(0[1-9]|(1|2)[0-9]|3[0-1]){1}.(0[1-9]{1}|1[0-2]{1}){1}.(19[0-9]{2}|2[0-9]{3})$');
            return r.test(value);
        },
        password: function (value, minRequiredPasswordLength, minNonAlphanumericChars) {
            var r = new RegExp(('(?=^[!@#$%\\^&*()_\\-+=\\[{\\]};:<>|\\./?a-zA-Z\\d]{' + minRequiredPasswordLength + ',}$)(?=([!@#$%\\^&*()_\\-+=\\[{\\]};:<>|\\./?a-zA-Z\\d]*\\W+){' + minNonAlphanumericChars + ',})[!@#$%\\^&*()_\\-+=\\[{\\]};:<>|\\./?a-zA-Z\\d]*$'));
            return r.test(value);
        }
    },
    Utils: {
        init: function (activeController, activeAction, activeParam, siteUrl) {
            $.ajaxSetup({ cache: false });
            BS3.activeController = activeController;
            BS3.activeAction = activeAction;
            BS3.activeParam = activeParam;
            BS3.siteUrl = siteUrl;
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
            });
        },
        openNavBarDropDownMenuOnHover: function () {
            $(function () {
                $(".navbar .dropdown").hover(
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
                );
            });
        },
        registerFileUpload: function () {
            $(function () {
                var fileUpload = function (element, options) {
                    this.$element = $(element);
                    this.$input = this.$element.find(':file');
                    if (this.$input.length === 0) return;
                    this.name = this.$input.attr('name') || options.name;
                    this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]');
                    if (this.$hidden.length === 0) {
                        this.$hidden = $('<input type="hidden">').insertBefore(this.$input);
                    }
                    this.original = {
                        exists: this.$element.hasClass('fileinput-exists'),
                        hiddenVal: this.$hidden.val()
                    }
                    this.listen();
                }

                fileUpload.prototype.listen = function () {
                    this.$input.on('change.bs.fileinput', $.proxy(this.change, this));
                    $(this.$input[0].form).on('reset.bs.fileinput', $.proxy(this.reset, this));
                    this.$element.find('[data-trigger="fileinput"]').on('click.bs.fileinput', $.proxy(this.trigger, this));
                    this.$element.find('[data-dismiss="fileinput"]').on('click.bs.fileinput', $.proxy(this.clear, this));
                },

                fileUpload.prototype.change = function (e) {
                    var files = e.target.files === undefined ? (e.target && e.target.value ? [{ name: e.target.value.replace(/^.+\\/, '') }] : []) : e.target.files;
                    e.stopPropagation();

                    if (files.length === 0) {
                        this.clear();
                        return;
                    }

                    this.$hidden.val('');
                    this.$hidden.attr('name', '');
                    this.$input.attr('name', this.name);

                    var file = files[0];
                    this.$element.find('.fileinput-filename').text(file.name);
                    this.$element.addClass('fileinput-exists').removeClass('fileinput-new');
                    this.$element.trigger('change.bs.fileinput');
                },

                fileUpload.prototype.clear = function (e) {
                    if (e) e.preventDefault();
                    this.$hidden.val('');
                    this.$hidden.attr('name', this.name);
                    this.$input.attr('name', '');

                    if (BS3.Utils.isIE()) {
                        var inputClone = this.$input.clone(true);
                        this.$input.after(inputClone);
                        this.$input.remove();
                        this.$input = inputClone;
                    } else {
                        this.$input.val('');
                    }

                    this.$element.find('.fileinput-filename').text('');
                    this.$element.addClass('fileinput-new').removeClass('fileinput-exists');

                    if (e !== undefined) {
                        this.$input.trigger('change');
                        this.$element.trigger('clear.bs.fileinput');
                    }
                },

                fileUpload.prototype.reset = function () {
                    this.clear();
                    this.$hidden.val(this.original.hiddenVal);
                    this.$element.find('.fileinput-filename').text('');

                    if (this.original.exists) {
                        this.$element.addClass('fileinput-exists').removeClass('fileinput-new');
                    } else {
                        this.$element.addClass('fileinput-new').removeClass('fileinput-exists');
                    }
                    this.$element.trigger('reset.bs.fileinput');
                },

                fileUpload.prototype.trigger = function (e) {
                    this.$input.trigger('click');
                    e.preventDefault();
                }

                var old = $.fn.fileinput;
                $.fn.fileinput = function (options) {
                    return this.each(function () {
                        var $this = $(this),
                            data = $this.data('bs.fileinput');
                        if (!data) $this.data('bs.fileinput', (data = new fileUpload(this, options)));
                        if (typeof options == 'string') data[options]();
                    });
                }

                $.fn.fileinput.Constructor = fileUpload;
                $.fn.fileinput.noConflict = function () {
                    $.fn.fileinput = old;
                    return this;
                }

                $(document).on('click.fileinput.data-api', '[data-provides="fileinput"]', function (e) {
                    var $this = $(this);
                    if ($this.data('bs.fileinput')) return;
                    $this.fileinput($this.data());
                    var $target = $(e.target).closest('[data-dismiss="fileinput"],[data-trigger="fileinput"]');
                    if ($target.length > 0) {
                        e.preventDefault();
                        $target.trigger('click.bs.fileinput');
                    }
                });
            });
        },
        registerTypeahead: function (ctrlValueId, ctrlTextId, url, items, delay, showHintOnFocus,
                                     functionMatcher, functionSorter, functionUpdater, functionHighlighter, functionDisplayText) {
            $(function () {
                var labels = [];
                var mapped = {};
                var options = {
                    items: items,
                    delay: delay,
                    showHintOnFocus: showHintOnFocus,
                    source: function (query, process) {
                        $.ajax({
                            url: (typeof url == 'function' ? url.call() : url),
                            dataType: 'json',
                            data: {
                                query: query
                            },
                            success: function (data) {
                                labels = [];
                                mapped = {};
                                if (data.code === 0) {
                                    data = data.message;
                                    $.each(data, function (i, item) {
                                        mapped[item.Text] = item.Value;
                                        labels.push(item.Text);
                                    });
                                } else {
                                    BS3.Utils.showError(data.message);
                                }
                                process(labels);
                            }
                        });
                    }
                };

                if (ctrlValueId) {
                    options.updater = function (item) {
                        $('#' + ctrlValueId).val(mapped[item]);
                        return item;
                    };
                }

                if (typeof functionMatcher == 'function') {
                    options.matcher = functionMatcher;
                }

                if (typeof functionSorter == 'function') {
                    options.sorter = functionSorter;
                }

                if (typeof functionUpdater == 'function') {
                    options.updater = functionUpdater;
                }

                if (typeof functionHighlighter == 'function') {
                    options.highlighter = functionHighlighter;
                }

                if (typeof functionDisplayText == 'function') {
                    options.displayText = functionDisplayText;
                }

                $('#' + ctrlTextId).typeahead(options);

                if (ctrlValueId) {
                    $('#' + ctrlTextId).on('change', function () {
                        var $el = $(this);
                        setTimeout(function () {
                            $('#' + ctrlValueId).val(mapped[$el.val()]);
                            $('#' + ctrlValueId).trigger('change');
                        }, 250);
                    });
                }
            });
        },
        registerMultipleSelect: function (ctrlId, isContains, url) {
            $(function () {
                $('#' + ctrlId).chosen({
                    search_contains: isContains,
                    ajax_url: url
                });
            });
        },
        registerFilterExpandCollapse: function (ctrlId, isIgnorePath) {
            isIgnorePath = (typeof isIgnorePath !== 'undefined' ? isIgnorePath : false);
            $(function () {
                $('#' + ctrlId).on('click', function () {
                    var caption = $(this).find('.pull-right i');
                    caption.toggleClass('fa-chevron-down fa-chevron-up');
                    $.cookie((isIgnorePath ? ctrlId : BS3.activeArea + BS3.activeController + BS3.activeAction + '_filter_state'),
                        (caption.hasClass('fa-chevron-up') ? 1 : 0),
                        {
                            expires: 365,
                            path: '/',
                            name: '_filter-cookie'
                        });
                });
            });
        },
        appliedFilterIndicator: function () {
            var label = $('#filter_caption .fa-filter');
            if ($('#filter_body').is(":hidden") && $('#filter_caption .badge').length > 0) {
                if (label.css('color') === "rgb(255, 0, 0)") {
                    label.css('color', 'rgb(0, 0, 0)');
                } else {
                    label.css('color', 'rgb(255, 0, 0)');
                }
            } else {
                label.css('color', 'rgb(0, 0, 0)');
            }
        },
        registerTextareaLimit: function (message) {
            $(function () {
                $('.limited').each(function () {
                    var ctrl = $(this);
                    if (ctrl.is('textarea')) {
                        var ctrlCountId = 'limit_' + $(this).attr('id');
                        ctrl.parent().append((!message ? BS3.Constants.limitMessage : message) + ' <span id="' + ctrlCountId + '"></span>');
                        ctrl.on('keyup', function () {
                            $('#' + ctrlCountId).html(function () {
                                var limit = ctrl.attr('class').match(/limit_[\d]+/);
                                var total = limit[0].split('_')[1];
                                var left = total - ctrl.val().length;
                                if (left < 0) {
                                    ctrl.val(ctrl.val().substr(0, total));
                                    left = 0;
                                }
                                return left;
                            });
                        });
                        ctrl.trigger('keyup');
                    }
                });
            });
        },
        registerDatepicker: function (selector, locale, format) {
            $(function () {
                $(selector).datetimepicker({
                    locale: locale !== undefined ? locale : 'en',
                    format: format !== undefined ? format : 'MM/DD/YYYY'
                });
            });
        },
        registerFormValidation: function (ctrlId, container) {
            $(function () {
                container = (typeof container !== 'undefined' ? container : 'form');
                $(container + ' .form-group.required').each(function () {
                    BS3.Private.addRequiredValidation($(this));
                });
                $(container + ' .form-group.integer').each(function () {
                    BS3.Private.addIntegerValidation($(this));
                });
                $(container + ' .form-group.decimal').each(function () {
                    BS3.Private.addDecimalValidation($(this));
                });
                $(container + ' .form-group.email').each(function () {
                    BS3.Private.addEmailValidation($(this));
                });
                $(container + ' .form-group.password').each(function () {
                    BS3.Private.addPasswordValidation($(this));
                });
                $(container + ' .form-group.time').each(function () {
                    BS3.Private.addTimeValidation($(this));
                });
                $(container + ' .form-group.diff_time').each(function () {
                    BS3.Private.addDiffTimeValidation($(this));
                });
                $(container + ' .form-group.date').each(function () {
                    BS3.Private.addDateValidation($(this));
                });
                $(container + ' .form-group.complex').each(function () {
                    BS3.Private.addComplexValidation($(this));
                });
                $('#' + ctrlId).off('click').on('click', function () {
                    $.each($(container).find('.nav-tabs a'), function (i, tabLink) {
                        $(tabLink).css('color', '#0088cc');
                    });
                    var errorGroup = null;
                    $(container + ' .form-group.required, ' +
                        container + ' .form-group.integer, ' +
                        container + ' .form-group.decimal, ' +
                        container + ' .form-group.email, ' +
                        container + ' .form-group.password, ' +
                        container + ' .form-group.time,' +
                        container + ' .form-group.diff_time,' +
                        container + ' .form-group.date,' +
                        container + ' .form-group.complex').each(function () {
                            BS3.Utils.validate($(this).data('control'));
                            if ($(this).hasClass('has-error') && errorGroup == null) {
                                errorGroup = $(this);
                            }
                        });
                    if (errorGroup) {
                        var tabId = errorGroup.parents('.tab-pane').attr('id');
                        if (tabId) {
                            var tab = $('ul.nav-tabs li a[href=#' + tabId + ']');
                            tab.tab('show');
                            tab.css('color', 'red');
                        }
                    }
                    return ($(container + ' .form-group.has-error').length === 0);
                });
            });
        },
        isIE: function () {
            var myNav = navigator.userAgent.toLowerCase();
            if ((myNav.indexOf('msie') !== -1)) {
                return parseInt(myNav.split('msie')[1]);
            } else {
                return (myNav.indexOf('MSIE')) ? parseInt(myNav.split('MSIE')[1]) : false;
            }
        },
        clearError: function (ctrlId) {
            var group = $('.form-group[data-control=' + ctrlId + ']');
            BS3.Private.clearError(group);
        },
        clearErrors: function (container) {
            container = (typeof container !== 'undefined' ? container : 'form');
            $(container + ' .form-group').each(function () {
                BS3.Private.clearError($(this));
            });
        },
        hasErrors: function (container) {
            container = (typeof container !== 'undefined' ? container : 'form');
            return ($(container + '.form-group.has-error').length !== 0);
        },
        clearControls: function (container) {
            container = (typeof container !== 'undefined' ? container : 'form');
            $(container).find('input,select,textarea').each(function () {
                $(this).val('');
            });
        },
        clearRequired: function (ctrlId) {
            var group = $('.form-group[data-control=' + ctrlId + ']');
            group.removeClass('required');
            BS3.Private.clearRequiredValidation(group);
            BS3.Private.clearError(group);
        },
        addRequired: function (ctrlId) {
            var group = $('.form-group[data-control=' + ctrlId + ']');
            group.addClass('required');
            BS3.Private.addRequiredValidation(group);
        },
        controlHide: function (ctrlId) {
            $('.form-group[data-control=' + ctrlId + ']').hide();
        },
        controlShow: function (ctrlId) {
            $('.form-group[data-control=' + ctrlId + ']').show();
        },
        validate: function (control) {
            if (control.charAt(0) !== '.') {
                control = '#' + control;
            }
            $(control).trigger('change.validation');
        },
        isId: function (value) {
            return /^[0-9]+$/.test(value);
        },
        showMessage: function (message) {
            swal({ title: "Message!", text: message, type: "info", html: true });
        },
        showError: function (message) {
            swal({ title: "Error!", text: message, type: "error", html: true });
        },
        areYouSure: function (url, confirmMessage) {
            confirmMessage = (typeof confirmMessage !== 'undefined' ? confirmMessage : BS3.Constants.confirmMessage);
            BS3.Utils.processConfirm(confirmMessage, url);
            return false;
        },
        processConfirm: function (message, url, target) {
            swal({
                title: "Warning",
                text: message,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                html: true
            }, function () {
                if (typeof url == 'function') {
                    url.call();
                } else {
                    target = (typeof target !== 'undefined' ? target : '');
                    if (target !== '') {
                        window.open(url, target);
                    } else {
                        location.href = url;
                    }
                }
            });
        },
        bindTable: function (containerId, templateId, data) {
            $('#' + containerId).html('');
            if (data.message.length > 0) {
                $('#' + templateId).tmpl(data).appendTo('#' + containerId);
                $('#' + containerId + ' .icon-pencil').tooltip({
                    placement: 'top'
                });
                $('#' + containerId + ' .icon-trash').tooltip({
                    placement: 'top'
                });
            }
        },
        bindDropDown: function (ctrlId, data) {
            $('#' + ctrlId).find('option').remove();
            if (data != null) {
                $.each(data.message, function (i, item) {
                    $('#' + ctrlId).append($('<option></option>').attr('value', item.id).text(item.name));
                });
            }
            $('#' + ctrlId).val('');
        },
        clearDropDown: function (ctrlId) {
            BS3.Utils.bindDropDown(ctrlId, null);
        },
        bindMessage: function (template, options) {
            return $('<div />').append($.tmpl(template, options)).html();
        },
        addCommas: function (value) {
            value += '';
            var x = value.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ' ' + '$2');
            }
            return x1 + x2;
        },
        formatNumber: function (value, decimals) {
            decimals = (typeof decimals !== 'undefined' ? decimals : 2);
            return BS3.Utils.addCommas((isNaN(value) ? 0 : value).toFixed(decimals));
        },
        toFixed: function (value, decimals) {
            decimals = (typeof decimals !== 'undefined' ? decimals : 2);
            return (isNaN(value) ? 0 : value).toFixed(decimals);
        },
        ifNull: function (value, nullValue) {
            return 1.00 * (isNaN(value) ? nullValue : value);
        },
        RegisterStringFormat: function () {
            String.format = function () {
                var theString = arguments[0];
                for (var i = 1; i < arguments.length; i++) {
                    var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
                    theString = theString.replace(regEx, arguments[i]);
                }
                return theString;
            };
        },
        RegisterSpinner: function () {
            $('.spinner .btn:first-of-type').on('click', function () {
                var input = $(this).parents('.input-group').find('input');
                var max = input.data('max-value');
                var increment = input.data('increment-value');
                var next = parseFloat(input.val(), 10) + increment;
                if (next <= max) {
                    input.val(next);
                    input.trigger('change');
                }
            });
            $('.spinner .btn:last-of-type').on('click', function () {
                var input = $(this).parents('.input-group').find('input');
                var min = input.data('min-value');
                var increment = input.data('increment-value');
                var prev = parseFloat(input.val(), 10) - increment;
                if (prev >= min) {
                    input.val(prev);
                    input.trigger('change');
                }
            });
        }
    },
    Private: {
        clearError: function (group) {
            group.removeClass('has-error');
            group.find('.error').remove();
        },
        addError: function (group, message) {
            var errorIcon = BS3.Private.renderTemplate(BS3.Constants.errorIcon, {
                title: message,
                site_url: BS3.siteUrl
            });
            var errorMessage = BS3.Private.renderTemplate(BS3.Constants.errorIconWithMessage, {
                message: message,
                site_url: BS3.siteUrl
            });
            var iconContainer = group.data('icon-container');
            var isMessage = group.data('is-message');
            group.addClass('has-error');
            if (iconContainer !== undefined) {
                $('#' + iconContainer).empty();
                if (isMessage !== undefined) {
                    $('#' + iconContainer).append(errorMessage);
                } else {
                    $('#' + iconContainer).append(errorIcon);
                }
            } else {
                group.append(errorIcon);
            }
            group.find('img.error').tooltip({
                html: true
            });
        },
        renderTemplate: function (template, options) {
            return $('<div />').append($.tmpl(template, options)).html();
        },
        space: function (value) {
            return value.replace(/\s/gm, '&nbsp;');
        },
        clearRequiredValidation: function (group) {
            $('#' + group.data('control')).off('change.validation.required');
        },
        addRequiredValidation: function (group) {
            var control = group.data('control');
            if (control.charAt(0) !== '.' && control.charAt(0) !== '#') {
                control = control.replace('.', '_');
                control = '#' + control;
            }
            $(control).on('change.validation.required', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                var condition = group.data('required-condition');
                if ($(this).is(":visible") && ($.trim($(this).val()) === '') && (condition ? eval(condition) : true)) {
                    var message = group.data('required-message');
                    if (!message) {
                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.requiredMessage), {
                            field_name: group.find('label').text()
                        });
                    }
                    BS3.Private.addError(group, message);
                    e.result = true;
                }
            });
        },
        addIntegerValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.integer', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    var message = '';
                    if (!BS3.Validate.integer($(this).val())) {
                        message = group.data('integer-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    } else {
                        if (group.data('not-equal') !== '' && $(this).val() === group.data('not-equal')) {
                            message = group.data('not-equal-message');
                            if (!message) {
                                message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.notEqualMessage), {
                                    field_name: group.find('label').text(),
                                    value: group.data('not-equal')
                                });
                            }
                            BS3.Private.addError(group, message);
                            e.result = true;
                        } else {
                            if ((group.data('min') !== '' && $(this).val() < group.data('min')) ||
                                (group.data('max') !== '' && $(this).val() > group.data('max'))) {
                                message = group.data('range-message');
                                if (!message) {
                                    if (group.data('min') && group.data('max')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeBetweenMessage), {
                                            field_name: group.find('label').text(),
                                            value_min: group.data('min'),
                                            value_max: group.data('max')
                                        });
                                    } else if (group.data('min')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeFromMessage), {
                                            field_name: group.find('label').text(),
                                            value_min: group.data('min')
                                        });
                                    } else if (group.data('max')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeToMessage), {
                                            field_name: group.find('label').text(),
                                            value_max: group.data('max')
                                        });
                                    }
                                }
                                BS3.Private.addError(group, message);
                                e.result = true;
                            }
                        }
                    }
                }
            });
        },
        addDecimalValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.decimal', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    var message = '';
                    if (!BS3.Validate.decimal($(this).val(), group.data('precision'), group.data('scale'))) {
                        message = group.data('decimal-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    } else {
                        if (group.data('not-equal') !== '' && $(this).val() === group.data('not-equal')) {
                            message = group.data('not-equal-message');
                            if (!message) {
                                message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.notEqualMessage), {
                                    field_name: group.find('label').text(),
                                    value: group.data('not-equal')
                                });
                            }
                            BS3.Private.addError(group, message);
                            e.result = true;
                        } else {
                            if ((group.data('min') !== '' && $(this).val() < group.data('min')) ||
                                (group.data('max') !== '' && $(this).val() > group.data('max'))) {
                                message = group.data('range-message');
                                if (!message) {
                                    if (group.data('min') && group.data('max')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeBetweenMessage), {
                                            field_name: group.find('label').text(),
                                            value_min: group.data('min'),
                                            value_max: group.data('max')
                                        });
                                    } else if (group.data('min')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeFromMessage), {
                                            field_name: group.find('label').text(),
                                            value_min: group.data('min')
                                        });
                                    } else if (group.data('max')) {
                                        message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.rangeToMessage), {
                                            field_name: group.find('label').text(),
                                            value_max: group.data('max')
                                        });
                                    }
                                }
                                BS3.Private.addError(group, message);
                                e.result = true;
                            }
                        }
                    }
                }
            });
        },
        addEmailValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.email', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    if (!BS3.Validate.email($(this).val())) {
                        var message = group.data('email-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    }
                }
            });
        },
        addPasswordValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.password', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    if (!BS3.Validate.password($(this).val(), group.data('password-min-length'), group.data('password-min-non-alphanumeric-characters'))) {
                        var message = group.data('password-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.passwordSecurityMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    }
                }
            });
        },
        addTimeValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.time', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    if (!BS3.Validate.time($(this).val())) {
                        var message = group.data('time-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    }
                }
            });
        },
        addDiffTimeValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.diff_time', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    if (!BS3.Validate.diff_time($(this).val())) {
                        var message = group.data('time-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    }
                }
            });
        },
        addDateValidation: function (group) {
            $('#' + group.data('control')).on('change.validation.date', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                if ($.trim($(this).val()) !== '') {
                    if (!BS3.Validate.date($(this).val())) {
                        var message = group.data('date-message');
                        if (!message) {
                            message = BS3.Private.renderTemplate(BS3.Private.space(BS3.Constants.formatMessage), {
                                field_name: group.find('label').text()
                            });
                        }
                        BS3.Private.addError(group, message);
                        e.result = true;
                    }
                }
            });
        },
        addComplexValidation: function (group) {
            var control = group.data('control');
            if (control.charAt(0) !== '.' && control.charAt(0) !== '#') {
                control = '#' + control;
            }
            $(control).on('change.validation.complex', function (e) {
                if (e.result) return e.result;
                BS3.Private.clearError(group);
                var condition = group.data('condition');
                if (typeof eval(condition) == 'function') {
                    var message = eval(condition).call();
                    switch (message) {
                        case true:
                            break;
                        case false:
                            message = group.data('complex-message');
                            BS3.Private.addError(group, message);
                            e.result = true;
                            break;
                        default:
                            BS3.Private.addError(group, message);
                            e.result = true;
                            break;
                    }
                }
            });
        }
    },
    Constants: {
        collapse: 'Collapse',
        expand: 'Expand',
        errorIcon: '<i class="error fa fa-warning" rel="tooltip" title="${title}"></i>',
        errorIconWithMessage: '<div class="error alert alert-error">${message}</div>',
        confirmMessage: 'Are you sure, that you want to delete this item?',
        requiredMessage: 'Field \'${field_name}\' is required.',
        formatMessage: 'Field \'${field_name}\' has incorrect format.',
        passwordSecurityMessage: 'Password is too simple.',
        notEqualMessage: 'Field \'${field_name}\' must be equal ${value}.',
        rangeFromMessage: 'Field \'${field_name}\' must be equal or greater than ${value_min}.',
        rangeToMessage: 'Field \'${field_name}\' must be equal or lower than ${value_max}.',
        rangeBetweenMessage: 'Field \'${field_name}\' must be beetwen ${value_min} и ${value_max}.',
        limitMessage: 'Characters left:'
    }
}