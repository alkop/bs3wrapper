﻿using BS3Wrapper.Demo.App_Start;

using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BS3Wrapper.Demo
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
