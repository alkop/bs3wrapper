﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace BS3Wrapper.Demo.Controllers
{
    using Models;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new WrapperViewModel
                {
                    Id = 1,
                    UserName = "Test User",
                    Email = "wrapper@google.com",
                    Password = "passwd",
                    Date = new DateTime(2015, 2, 13),
                    Time = DateTime.Now,
                    DecimalValue = 10.25m,
                    IntValue = 10,
                    SelectedId = 2
                };

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(WrapperViewModel model)
        {
            return View(model);
        }

        public JsonResult GetTypeaheadSelectList(string query)
        {
            return Json(new { code = 0, message = new[] { new SelectListItem { Value = "1", Text = "Option 1" },
                                                          new SelectListItem { Value = "2", Text = "Option 2" },
                                                          new SelectListItem { Value = "3", Text = "Option 3" },
                                                          new SelectListItem { Value = "4", Text = "Option 4" },
                                                          new SelectListItem { Value = "5", Text = "Option 5" },
                                                          new SelectListItem { Value = "6", Text = "Option 6" },
                                                          new SelectListItem { Value = "7", Text = "Option 7" },
                                                          new SelectListItem { Value = "8", Text = "Option 8" }
                                                        }.Where(t => t.Text.Contains(query))
                             },
                        JsonRequestBehavior.AllowGet);
        }
    }
}