﻿using System;
using System.ComponentModel;
using System.Web;

namespace BS3Wrapper.Demo.Models
{
    using Attributes.Validation;

    public class WrapperViewModel
    {
        public int Id { get; set; }

        [DisplayName("User name")]
        [Required]
        public string UserName { get; set; }

        [Required]
        [Email]
        public string Email { get; set; }

        [Required]
        [Password(MinRequiredNonAlphanumericCharacters = 0, ErrorMessage = "Password must be at least 7 characters!")]
        public string Password { get; set; }

        public DateTime Date { get; set; }

        public DateTime Time { get; set; }

        [Range(1, 12)]
        public int IntValue { get; set; }

        [Decimal]
        public decimal DecimalValue { get; set; }

        [Required]
        public int SelectedId { get; set; }

        public HttpPostedFileBase File { get; set; }

        public bool IsVisible { get; set; }

        public int TypeaheadOptionKey { get; set; }

        public string TypeaheadOptionText { get; set; }
    }
}