BS3Wrapper is set of HTML helpers for ASP.NET MVC
=================================================

[![Build status][appveyor img]][appveyor]
[![NuGet][nuget img]][nuget]
[![Gitter][gitter img]][gitter]
[![License][license img]][license]

* Designed for faster and easy create UI
* Integrated with DateTimePicker, SweetAlerts, Typeahead etc..
* Custom client-side validation
* Available at NuGet: 
    * `PM> Install-Package BS3Wrapper`
* Allow You to replace large HTML into few lines of code
* For full documentation and more samples go to [Wiki Page][WikiHome]

## Basic usage
### Bootstrap html:
```
#!html
 
<div class="form-group">
   <label class="control-label col-sm-2" for="Email">Email</label>
   <div class="col-sm-3">
      <input class="form-control" id="Email" name="Email" type="text">
   </div>
</div>

```

### BS3Wrapper code:
   
```
#!c#

@Html.BS3().TextBoxGroupFor(m => m.Email).ControlWidth(Width.ColSm3)	

```


[WikiHome]: https://bitbucket.org/alkop/bs3wrapper/wiki/

[appveyor]:https://ci.appveyor.com/project/AlexanderKopylov/bs3wrapper/branch/develop
[appveyor img]:https://ci.appveyor.com/api/projects/status/ia0skirudubagcw5/branch/develop?svg=true

[nuget]:https://www.nuget.org/packages/bs3wrapper
[nuget img]:https://img.shields.io/nuget/v/bs3wrapper.svg

[gitter]:https://gitter.im/alkop/bs3wrapper
[gitter img]:https://img.shields.io/gitter/room/nwjs/nw.js.svg

[license]:LICENSE.md
[license img]:https://img.shields.io/badge/license-MIT-blue.svg